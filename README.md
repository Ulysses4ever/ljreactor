The work presented here addresses all four paragraphs of Exhibit A from Professional services Agreement.

1. After comprehensive study of `Pkg` source code and the algorithms used there we decided that the best way to handle dependencies of `Pkg.add` is to execute `Pkg.add` in advance, before actual logging. This idea was implemented in `src/collecl_logs.py`.

2. Not only we identified all workarounds implemented in LambdaJulia parser, but replaced them with concise code leveraging recently added patch to Julia's pretty printer that allows for parseable type representations. Corresponding changes were made to `src/parsing.jl` and `src/decl_dumps.jl`.

3. According to par. 1, changes were made to the `src/collecl_logs.py` script.

4. Logging script is thread-safe now due to a) the proper usage of the debug output from inside the Julia VM (before is was not due to non-blocking mode for `stderr`); b) ruling out most of the package manegement work out of logging process (so called, add-programs now only do `using` statement and not `Pkg.add`, because, as mentioned in par. 1, `Pkg.add` is done in advance; test-programs only `include` test file of the package, instead of `Pkg.test`, which is equivalent given that we install all the test dependencies in advance now). Corresponding fixes were brought a) to the logging infrastructure (cf. `julia-log-06rc2/src/subtype.c`), b) to the `src/collecl_logs.py` script and new, `src/install_test_deps.jl` one.

