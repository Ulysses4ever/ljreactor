include("AST.jl")
include("env.jl")

########################  Pseudo Base type declarations     ######################

### Pseudo-decls for types from Base library
#
# Choose 0 for simple bootstrap (about a dozen of type declarations,
#        1 for loading full dump of pseudo Base type declarations (can be slow):
#        2 for loading full dump WITH function types
#        3 custom decls dump: provide filename in the var:
#           decls_dump_file

if !isdefined(:decls_mode)
    decls_mode = 1
end

if decls_mode == 0
    base_types_decls_fname = joinpath(dirname(@__FILE__()), "decls_minimal.jl")
elseif decls_mode == 1
    base_types_decls_fname = joinpath(dirname(@__FILE__()), "decls_base_inferred.json")
elseif decls_mode == 2 # custom dump: should be stored in decls_dump_file
    base_types_decls_fname = decls_dump_file
else
    throw(ErrorException("Unsupported decls_mode value"))
end

# some etra decls -- mostly to simulate Julia type aliases
decls_extra_fname = joinpath(dirname(@__FILE__()), "decls_extra.jl")

######################       Auxiliary functions       ######################

function compose_unionalls(n::Int)
  n == 0 ? TDataType() : TUnionAll(compose_unionalls(n-1))
end
  
function flatten(x)
  [i for i in vcat(x...)]
end

# Check if given type node is TUnionAll
lj_is_unionall(t :: TUnionAll) = true
lj_is_unionall(t :: ASTBase) = false

struct LJErrNameNotFound <: Exception
  name :: String
end

# Lookup for type declaration by type name
# Throws an exception if the name is not known
function lj_lookup(t :: TName, tds :: TyDeclCol)
    key = "$(t)"
    found = true
    while !haskey(tds, key)
        i   = search(key, '.')
        if i == 0
          found = false
          break
        end
        key = key[i+1:end]
    end
    try
      if !found
        tds[key[search(key, ':'):end]]
      else
        tds[key]
      end
    catch e
        throw(LJErrNameNotFound("$(t)"))
    end
end

# Return the number of type parameters for a given name
lj_lookup_params_cnt(t :: TName, tds :: TyDeclCol) = 
    length(lj_lookup(t, tds).params)

### Check if something is a Symbol
is_symbol(t) = false
is_symbol(t :: Symbol) = true

function lj_sub_no_freevars(x,y,tds)
  if x == y
    return true
  elseif isempty(free_variables(x)) && isempty(free_variables(y))
    return lj_subtype(x,y,tds).sub
  else
    return false
  end
end

# Simplified representation of a union: collapse all pairs subtype:supertype
# to a supertype

# FZN: This should mimick the code of simple_join in subtype.c;
# collapsing all pairs is incorrect.

function simplify_union(ts, tds, env)
  if isempty(ts)
    return ts
  end
  ts1 =
        reduce(
          (r,t) -> simple_join2(r, t, tds, env), 
          ts)
  #println("simpl-union: $(ts1)")
  no_union(ts1)
end

function simple_join2(t1::ASTBase, t2::ASTBase, tds, env)
  #println("Hi, simple_join2, inp:\n\t$(t1), $(t2)")
  if t1 == EmptyUnion || t2 == TAny || t1 == t2
    #println("1")
    return t2
  elseif t2 == EmptyUnion || t1 == TAny
    #println("2")
    return t1
  elseif !(is_type(t1, tds, env) || is_typevar(t1)) ||
         !(is_type(t2, tds, env) || is_typevar(t2))
    #println("3")
    return TAny()
  elseif in_union(t1, t2)
    #println("4")
    return t1
  elseif in_union(t2, t1)
    #println("5")
    return t2
#    if (jl_is_kind(a) && jl_is_type_type(b) && jl_typeof(jl_tparam0(b)) == a)
#        return a;
#    if (jl_is_kind(b) && jl_is_type_type(a) && jl_typeof(jl_tparam0(a)) == b)
#       return b;
  elseif !has_free_vars(t1) && !has_free_vars(t2)
    if lj_subtype(t1, t2, tds).sub
      #println("6")
      return t2
    end
    if lj_subtype(t2, t1, tds).sub
      #println("7")
      return t1
    end
  end
  #println("8")
  TUnion([t1, t2])
end

is_type(t, tds, env) = is_kind(lj_typeof(t, tds, env))

is_typevar(:: ASTBase) = false
is_typevar(:: TVar) = true

in_union(t1 :: ASTBase, t2 :: ASTBase) = t1 == t2

function in_union(t1 :: TUnion, t2 :: ASTBase)
  any(t -> in_union(t, t2), t1.ts)
end

has_free_vars(t) = length(free_variables(t)) != 0

#
#  Simplify representation of a type (as in sec. 3 of the LabdaJulia paper)
#
function simplify(t :: ASTBase, :: Any, ::Env)
    #return t
    throw(ErrorException("`simplify` is partial: $(t)"))
end

# Leaf cases
simplify(t :: TDataType, :: Any, ::Env) = t
simplify(t :: TUnionAll, :: Any, ::Env) = t
simplify(t :: TSuperUnion, :: Any, ::Env) = t
simplify(t :: TSuperTuple, :: Any, ::Env) = t
simplify(t :: TAny, :: Any, ::Env) = t
simplify(t :: TVar, :: Any, ::Env) = t
simplify(t :: TName, :: Any, ::Env) = t
simplify(t :: TInt, :: Any, ::Env) = t

simplify(t :: TType, tds:: Any, env::Env) = 
    TType(simplify(t.t, tds, env))
    
simplify(t :: TApp, tds :: Any, env::Env) = 
    TApp(simplify(t.t, tds, env), map(t -> simplify(t, tds, env), t.ts))
    
simplify(t :: TTuple, tds :: Any, env::Env) = 
    TTuple(map(t -> simplify(t, tds, env), t.ts))

function simplify(t :: TUnion, tds :: Any, env :: Env)
    #println("simpl(TUnion) inp: $(t)")
    #dump(t)
    ts = map(t -> simplify(t, tds, env), t.ts)
    #dump(ts)
    ts1 = simplify_union(ts, tds, env)
    #dump(ts1)
    if length(ts1) != 1 # 0, 2, 3, ...
      res = TUnion(ts1)
    else
      res = ts1[1]
    end
    #println("simpl(TUnion) res: $(res)")
    #dump(res)
    res
end

function simplify(t :: TWhere, tds :: Any, env :: Env)
    # Env management
    #println("simpl(TWhere) inp: $(t)")
    # dump(t)
    tv = t.tvar
    tt = t.t
    if env_conflict(env, t.tvar)
      (tv, tt) = freshen(env, t.tvar, t.t)
    end
    #println("env: $(env)")
    env1 = copy(env)
    env_add!(env1, tv, t.lb, t.ub, TagLeft())  # Tag does not matter

    simpl_t = simplify(t.t, tds, env1)
    #println("simpl head:")
    #dump(simpl_t)
    if isa(simpl_t, TVar)
      if simpl_t == t.tvar
        return t.ub
      end
    elseif findfirst(free_variables(simpl_t), t.tvar.sym) == 0
      return simpl_t
    end
    res = TWhere(simpl_t, t.tvar, t.lb, t.ub)
    #println("simpl(TWhere) res: $(res)")
    #dump(res)
    res
end

# returns the collection of types calculated via picking one element of each
# union at a time
function no_union(t :: ASTBase)
    lj_error(string("no_union not implemented for: ",t))
end

no_union(t :: TDataType) = [t]
no_union(t :: TAny) = [t]
no_union(t :: TName) = [t]
no_union(t :: TVar) = [t]
no_union(t :: TSuperTuple) = [t]
no_union(t :: TUnionAll) = [t]
no_union(t :: TSuperUnion) = [t]
no_union(t :: TType) = [t]

no_union(t :: TUnion) = reduce(vcat, Vector{ASTBase}(), map(no_union, t.ts))

# A: We are unsure what to do with these two:
no_union(t :: TApp) = [t]
no_union(t :: TWhere) = [t]

function no_union(t :: TTuple)
    rec = Vector{Vector{ASTBase}}()
    reduce((_, t) -> push!(rec, no_union(t)), rec, t.ts)

    r = cartesian(rec)

    res = Vector{ASTBase}()
    reduce((_, ts) -> push!(res, TTuple(ts)), res, r)
    res
end

function lift_union(t::ASTBase)
  tl = no_union(t)
  if length(tl) == 1
    tl[1]
  else
    TUnion(tl)
  end
end

# Cartesian product of x[i]'s
function cartesian(x :: Vector{Vector{T}} where T)
    res = (typeof(x))()      # --> empty 2D array
    push!(res, eltype(x)())  # --> 1-element 2D array: [[]]
    if isempty(x)
        return res
    end
    pop!(res)                # again empty
    rec = cartesian(x[2:end])
    for i in 1:length(x[1])
        res = vcat(res, map(z -> vcat([x[1][i]], z), rec))
    end
    res
end

# is_kind
function is_kind(t::ASTBase)
  isa(t,TDataType) || isa(t,TSuperUnion) || isa(t,TUnionAll) || t == TName("Core.TypeofBottom")
end

# # -> HHHH only if not inside "..."
function replace_hashes_not_in_lits(s :: String)
    inlit = false
    res = ""
    for c in s
        if c == '#'
            res *= inlit ? "#" : "HHHH"
        else
            if c == '"'
                inlit = !inlit
            end
            res *= "$(c)"
        end
    end
    res
end

# A.B.C -> (C, A.B)
function split_last_dot(s :: String)
  rdot_i = rsearch(s, '.')
  (s[rdot_i+1:end], s[1:rdot_i-1])
end

