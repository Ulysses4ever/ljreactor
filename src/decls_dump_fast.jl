module Stats_UNIQUE_NAME_THIS_SHOULDNT_EXIST_ANYWHERE_ELSE
types_analysed = Set{Type}()
modules_analysed = Set{Module}()

function get_body(t)
    while isdefined(t, :body)
        t = t.body
    end
    t
end

function get_attr(t)
    if isleaftype(t)
        r = "struct"
        if !isimmutable(t)
            r = "mutable " * r
        end
        r
    else
        "abstract type"
    end
end

function print_ty_decl(f, t)
    if in(t, types_analysed)
        return
    else
        push!(types_analysed, t)
    end
    lhs = "$(get_body(t))"
    s = supertype(t)
    rhs = "$(get_body(s))"
    println(f, "  \"$(get_attr(t)) $(lhs) <: $(rhs) end\",")
end

function in_assumed_mod(m::Module)
    if (m == Base || m == Core)
        return true
    end
    if module_parent(m) != Main
        return in_assumed_mod(module_parent(m))
    end
    return false
end

function in_assumed_mod(t::Type)
    if typeof(t) == UnionAll
        return in_assumed_mod(t.body)
    else
        return in_assumed_mod(t.name.module)
    end
end
        

function print_types_in_module(f, m::Module)
    nf = 0

    if (in(m, modules_analysed))
        return
    else
        push!(modules_analysed, m)
    end


    for name in names(m, true)
        try
            obj = getfield(m, name)
        catch e
            continue
        end

        
        if (obj == Stats_UNIQUE_NAME_THIS_SHOULDNT_EXIST_ANYWHERE_ELSE || obj == m)
            continue
        end
        to = typeof(obj)
        if to == Module
            println(obj)
            print_types_in_module(f, obj)
            continue
        elseif isa(obj, DataType) || isa(Base.unwrap_unionall(obj), UnionAll)
            print_ty_decl(f, obj)
        elseif to <: Function
            print_ty_decl(f, to)
        end
        nf += 1
    end
end
end

if !isdefined(:fname_decls)
    fname_decls = length(ARGS) > 1 ? ARGS[1] : "decls_base_inferred.jl"
end
start_type = Any
open(fname_decls, "w") do f
    println(f, "base_types_decls = [")
    Stats_UNIQUE_NAME_THIS_SHOULDNT_EXIST_ANYWHERE_ELSE.print_types_in_module(f, Main)
    println(f, "]")
end

