include("type_validator.jl")
include("Monads.jl")
using Monads

#####  Checking utilities: compare results of LJ with ones from the logs  #####
#                       or sanity-check the logs themselves

# Turn String into a (Julia's) thing
reify(x :: String) = eval(parse(replace_hashes_not_in_lits(x)))

# Check lj_typeof
function check_typeof(ts :: String, tt :: Type{T}) where T
    u = typeof(tt)
    v = lj_typeof(ts)
    su = string(lj_parse_type("$(u)"))
    sv = "$(v)"
    r = su == sv ? 1 : 0
    r
end

# Logs should make sense from Julia's point of view
function check_log(t1, t2, refres :: Bool)
    refres == issubtype(t1, t2)
end

# Check lj_subtype
function check_subtype(t1 :: String, t2 :: String, refres :: Bool)
#    println("Check subtype: $(t1) <: $(t2)")
    lj_subtype(t1, t2).sub == refres ? 1 : 0
end

####################    Statistics utilities    ##############################

mutable struct Stats
    cnt :: Int
    pos :: Int
    neg :: Int
    exc :: Int
    nnf :: Int
    varg    :: Int
    tt      :: Int
    freevar :: Int
    undersc :: Int
    capany  :: Int
    logfail :: Int
end
Stats() = Stats(0,0,0,0,0,0,0,0,0,0,0)
function compute_neg(s :: Stats)
#  println("b: ", s)
  s.neg = s.cnt - s.pos - s.exc - s.nnf - s.varg - s.tt - s.freevar - s.undersc - s.capany - s.logfail
#  println("a: ", s)
end

parser_s = Stats()
typeof_s = Stats()
subtype_s = Stats()

######################    Logging utilities   ##############################
#
# Not that logs! A new ones -- the results of a validation

function my_file_log_raw(f, t1, t2, r)
  println(f, t1, "\n", t2, "\n", r, "\n")
end

function my_file_log_subtype(f, t1, t2, r)
  println(f, t1, "\n", t2, "\n-> ", r, "\n")
end

function my_file_log_typeof(f, t)
  println(f, t)
end

function my_file_log_subtype(f, t1, t2, r, e)
  e1 = trunc("$(e)")
  println(f, t1, "\n", t2, "\n-> ", r, "\nError: $(e1)", "\n")
end

function my_file_log_typeof(f, t, e)
  e1 = trunc("$(e)")
  println(f, t, "\nError: $(e1)", "\n")
end

function trunc(s :: String)
  if length(s) > 200
    s = s[1:200] * "... (truncated for clarity)"
  end
  s
end

### Validation utilities: validation is checking (as above) + error handling ###

function validate_type(ts :: String)
  try
    t = reify(ts)
    Maybe(t)
  catch e
    if isa(e, UndefVarError) 
      my_file_log_typeof(exce_freevar_f, ts, e)
      parser_s.freevar += 1
    else 
      my_file_log_typeof(fail_log_f, ts, e)
      parser_s.logfail += 1
    end
    Maybe{Void}()
  end
end

function validate_res_format(res_str :: String, t1 :: String, t2 :: String)
    # Sanity check of logs #1: type-1 failure mean we found complete garbage
    # This should not happen after improvement of logging infrastructure
    if length(res_str) != 4 || !(res_str[4] in ['0', '1'])
        #println("WARNING: non-parsable result string")
        println(fail_log_f, "type-1 failure (corrupted log)")
        parser_s.logfail += 2
        my_file_log_raw(fail_log_f, t1, t2, res_str)
        Maybe{Bool}()
    else
        sub_res = (res_str[4] == '1')  # result of t1 <: t2
        Maybe(sub_res)
    end
end

# Sanity check of logs: Julia should agree on what we've found in logs
function validate_log(t1, t2, sub_res :: Bool)
    # tt1/2 should by Type{T} where T, but in some (ill-formed) cases can be smth else
    try
        cl = check_log(t1, t2, sub_res)
        #println("check_log($(t1), $(t2)): ", cl)
        if !cl
            throw(ErrorException("Julia does not agree with the log"))
        else
            return true
        end
    catch e
        println(fail_log_f, "type-2 failure")
        my_file_log_subtype(fail_log_f, t1, t2, sub_res ? '1' : '0', e)
        parser_s.logfail += 2
        #parser_s.pos -= 2
        return false
    end
end

# Is our parser good enough to handle a (surely meaningful, due to previious checks) 
# type found in a log?
function validate_parser(ts :: String)
    stat = parser_s
    result = true
    if contains(ts, "Vararg")
        result = false
        stat.varg += 1
    elseif contains(ts, "where _")
        result = false
        stat.undersc += 1
    else
        try
            lj_parse_type(ts)
            stat.pos += 1
        catch e
            result = false
            if isa(e, LJErrTermInType)
              stat.tt += 1
            else
              stat.exc += 1
              my_file_log_typeof(exce_parser_f, ts, e)
            end
        end
    end
    result
end

# Is our lj_typeof good? It has some restrictions, so we pass string to check 
# those along with actual type (which we will feed to Julia's typeof)
function validate_typeof(ts :: String, tt) 
    # tt should by Type{T} where T, but in some (ill-formed) cases can be smth else
    stat = typeof_s
    stat.cnt += 1
    if contains(ts, "ANY")
        stat.capany += 1
        return true
    end

    result = true
    try
        ct1 = check_typeof(ts, tt)
        stat.pos += ct1
        if ct1 == 0
            my_file_log_typeof(fail_typeof_f, ts)
        end
    catch e
        #println(ts, " exception:\n$(e)")
        result = false
        if isa(e, LJErrNameNotFound)
          my_file_log_typeof(exce_typeof_f, ts, "Name not found: $(e.name)")
          stat.nnf += 1
        else
          my_file_log_typeof(exce_typeof_f, ts, e)
          stat.exc += 1
        end
    end
    result
end

function validate_subtype(t1 :: String, t2 :: String, sub_res :: Bool)
    subtype_s.cnt += 1
    try
        sr = check_subtype(t1, t2, sub_res)
        subtype_s.pos += sr
        if sr == 0
            my_file_log_subtype(fail_subtype_f, t1, t2, sub_res)
        end
        true
    catch e
        subtype_s.exc += 1
        my_file_log_subtype(exce_subtype_f, t1, t2, sub_res, e)
        false
    end
end

# proc_type :: String -> Maybe Type
proc_type(s :: String) = @mdo Maybe begin
        t <| validate_type(s)
        vp = validate_parser(s)
        guard(vp && validate_typeof(s, t))
        return(t)
    end

# Top-level structure of the algorithm for log processing
function proc(s1, s2, rs)
    @mdo Maybe begin
        r  <| validate_res_format(rs, s1, s2)
        tt <| mapM(Maybe, proc_type, [s1, s2])
        vl = validate_log(tt[1], tt[2], r)
        guard(vl && validate_subtype(s1, s2, r))
    end
end

########################      Main       ########################

start = true # debug
if !start
    tralala
end

### Looking up log file name to process
if !isdefined(:log_name)
    log_name = "../logging/empty-prog/log_subt.txt"
end
if length(ARGS) == 0 || !isfile(ARGS[1])
    println("INFO: You haven't provided a trace file as either a" *
    " first command-line argument or `log_name` variable, " *
    "defaulting to $(log_name)")
else
    #println("we got the arg!")
    log_name = ARGS[1]
end

# Redirect stderr to /dev/null (non-portable) #=
if ispath("/dev/null")
   redirect_stderr(open("/dev/null"))
   println("WARNING: stderr was redirected to /dev/null for clarity")
end
# =#

### Main paths

base_no_ext(s) = basename(s)[1:end-4]

log_base = base_no_ext(log_name)
log_out_dir = joinpath(dirname(log_name), log_base)
res_fname = joinpath(log_out_dir, "results.txt")

if !isdir(log_out_dir)
    mkdir(log_out_dir)
end

# Open all log files
fail_subtype_f  = open(log_out_dir * "/subtype-failures.txt", "w" )
exce_subtype_f  = open(log_out_dir * "/subtype-exceptions.txt", "w" )
fail_typeof_f   = open(log_out_dir * "/typeof-failures.txt", "w" )
exce_typeof_f   = open(log_out_dir * "/typeof-exceptions.txt", "w" )
exce_parser_f   = open(log_out_dir * "/parser-exceptions.txt", "w" )
exce_freevar_f  = open(log_out_dir * "/freevar-exceptions.txt", "w" )
fail_log_f      = open(log_out_dir * "/log-failures.txt", "w" )

### Process

lns = readlines(log_name)
lenlns = length(lns)
workload = div(length(lns), 4)
wl_10per = div(workload, 10)
print("\nProgress: 0% ")
cnt = 1
cntw = 0

while start && cnt <= lenlns - 3

    # Rendering progress bar
    cntw += 1
    if wl_10per != 0 && mod(cntw, wl_10per) == 0
        print("$(div(cntw, wl_10per))0% ")
    end

    # Update values for new iteration
    parser_s.cnt += 2 # We asume we have two more types in the log, though
                      # they may be complete garbage (that's why parser_s and
                      # not typeof_s here)
    t1 = lns[cnt]            # type 1
    t2 = lns[cnt+1]          # type 2
    res_str = lns[cnt+2]
    cnt += 4 # empty line

    proc(t1, t2, res_str)
end

println()
close(fail_subtype_f)
close(exce_subtype_f)
close(fail_typeof_f)
close(exce_typeof_f)
close(exce_parser_f)
close(exce_freevar_f)
close(fail_log_f)

compute_neg(parser_s)
compute_neg(typeof_s)
compute_neg(subtype_s)
w = 7
dash ="--"
res_f  = open(res_fname, "w")
println(res_f, """
Tested: $(log_name)

            | Parser  | Typeof  | Subtype
------------------------------------------
Total       | $(lpad(parser_s.cnt, w)) | $(lpad(typeof_s.cnt, w)) | $(lpad(subtype_s.cnt, w))
Passed      | $(lpad(parser_s.pos, w)) | $(lpad(typeof_s.pos, w)) | $(lpad(subtype_s.pos, w))
Failed      | $(lpad(parser_s.neg, w)) | $(lpad(typeof_s.neg, w)) | $(lpad(subtype_s.neg, w))
Exceptions  | $(lpad(parser_s.exc, w)) | $(lpad(typeof_s.exc, w)) | $(lpad(subtype_s.exc, w))
Unkn names  | $(lpad(dash, w)) | $(lpad(typeof_s.nnf, w)) |   --
ANY         | $(lpad(dash, w)) | $(lpad(typeof_s.capany, w)) |   --
Varargs     | $(lpad(parser_s.varg, w)) | $(lpad(dash, w)) |   --
Underscores | $(lpad(parser_s.undersc, w)) | $(lpad(dash, w)) |   --
Term in type| $(lpad(parser_s.tt, w)) | $(lpad(dash, w)) |   --
Free var    | $(lpad(parser_s.freevar, w)) | $(lpad(dash, w)) |   --
Log failures| $(lpad(parser_s.logfail, w)) | $(lpad(dash, w)) |   --
""")
close(res_f)

using JSON

open(joinpath(log_out_dir,"results.json"), "w") do f
    JSON.print(f, Dict(   :parser  => parser_s
                        , :typeof  => typeof_s
                        , :subtype => subtype_s), 4)
end

#println(readstring(res_fname))
# =#
