import pkgs_star
import itertools
import os

out_f = open('pkgs_list.txt','w')

ps = sorted(pkgs_star.pkgs, key=lambda x: x[2])

pss = reversed(ps)
#pss = ps[-100:]
#pss = map(lambda x: x[1], ps)
#itertools.dropwhile(lambda x: x[2] < 15, ps)

i = 0

for p in itertools.imap(lambda x: x[1], pss):
    status = os.system('julia -e \'Pkg.add(\"{}\"); using {}\''.format(p, os.path.splitext(p)[0]))
    i = i+1
    if status == 0:
        print(str(i)+"] passed: "+p)
        out_f.write(p+'\n')
        out_f.flush()
    else:
        print(str(i)+"] failed: "+p)

out_f.close()
#print len(pss)
#print "prjs = {0}".format( pss )
