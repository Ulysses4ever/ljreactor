### state used for flags: counting occurrences, consistency_check_enabled, ...
struct ST_State
  in_consistency_check :: Bool
  occurrence_counting  :: Bool   # for diagonal rule
  covariant_position   :: Bool   # for diagonal rule
  invariant_position   :: Bool   # for diagonal rule
end

ST_initial_state = ST_State(false, true, false, false)

function state_set_in_consistency_check(state::ST_State)
  return ST_State(true, state.occurrence_counting, state.covariant_position, state.invariant_position)
end

function state_disable_occurrence_counting(state::ST_State)
  return ST_State(state.in_consistency_check, false, state.covariant_position, state.invariant_position)
end

function state_set_covariant_position(state::ST_State)
  return ST_State(state.in_consistency_check, state.occurrence_counting, true, false)
end

function state_set_invariant_position(state::ST_State)
  return ST_State(state.in_consistency_check, state.occurrence_counting, false, true)
end


# var environment for subtyping

abstract type EnvEntry end

abstract type VEnvTag end
struct TagLeft <: VEnvTag end
struct TagRight <: VEnvTag end

struct Occurs
  cov :: Int
  inv :: Int
end

import Base.+
import Base.zero

function (+)(o1::Occurs, o2::Occurs) 
  return (Occurs(o1.cov+o2.cov, o1.inv+o2.inv))
end

function zero(::Type{Occurs})
  return Occurs(0,0)
end

function show(io::IO, occ::Occurs)
  print(io,string("[",occ.cov,"|",occ.inv,"]"))
  print(io,"")
end

function increase_occ(occ::Occurs, state::ST_State)
  if state.occurrence_counting
    if state.covariant_position && !state.invariant_position
      return Occurs(occ.cov+1, occ.inv)
    elseif !state.covariant_position && state.invariant_position
      return Occurs(occ.cov, occ.inv+1)
    elseif !state.covariant_position && !state.invariant_position
      return Occurs(occ.cov, occ.inv)  # FZN: unsure about this
    else
      lj_error(string("Inconsistent state: ", state))
    end
  else
    return occ
  end
end


struct VEnv <: EnvEntry
    var :: TVar
    lb  :: ASTBase
    ub  :: ASTBase
    tag :: VEnvTag
    occ :: Occurs
end

function show(io::IO, venv::VEnv)
  print(io,"[",venv.var," ^",venv.ub," _",venv.lb," ",venv.tag," ", venv.occ, "] ")
end

function show(io::IO, ::TagLeft)
  print(io,"L")
end

function show(io::IO, ::TagRight)
  print(io,"R")
end

struct EnvBarrier <: EnvEntry end

function show(io::IO, ::EnvBarrier)
  print(io, "|Barrier| ")
end

struct Env
  curr :: Vector{EnvEntry}
  past :: Vector{VEnv}
end

import Base.copy
copy(e :: Env) = Env(copy(e.curr), copy(e.past))

function show(io::IO, env::Env)
  map(ee -> print(io,ee), env.past)
  print(io," ||| ")
  map(ee -> print(io,ee), env.curr)
end

# free variables in a venv

function free_variables(ee::VEnv)
  return flatten(vcat(ee.var.sym, free_variables(ee.lb), free_variables(ee.ub)))
end

function free_variables(ee::EnvBarrier)
  return []
end

function free_variables(env::Env)
  return flatten(vcat(map(ee -> free_variables(ee), env.curr),
                      map(ee -> free_variables(ee), env.past)))
end

# substitutions over environments (for var_left instantiation)

function substitute(ee::EnvBarrier, v::TVar, t::ASTBase)
  return ee
end

function substitute(ee::VEnv, v::TVar, t::ASTBase)
  if v == ee.var
    return ee
  else
    return VEnv(ee.var, substitute(ee.lb,v,t), substitute(ee.ub,v,t), ee.tag, ee.occ)
  end
end

function substitute(env::Env, v::TVar, t::ASTBase)
  env1 = Env(map(ee -> substitute(ee,v,t), env.curr), map(ee -> substitute(ee,v,t), env.past))
  return env1
end

### gensym and freshen

function lj_gensym(env::Env, v::TVar)
  fv = free_variables(env)
  i = 1
  while true
    if !(Symbol(v,i) in fv)
      return TVar(Symbol(v,i))
    end
    i = i+1
  end
end

function freshen(env::Env, v::TVar, t::ASTBase)
  vn = lj_gensym(env, v)
  t1 = rename(t,v,vn)
  debug_out(string("<Freshen>\n<v>",v,"</v>\n<vn>",vn,"</vn>\n</Freshen>"))
  return (vn,t1)
end

### add entry to env, possibly alpha-renaming if variable clash

function env_conflict(env::Env, v::TVar)
  if v.sym in free_variables(env)
    return true
  else
    return false
  end
end

function env_add!(env::Env, eb::EnvBarrier)
  append!(env.curr, [eb])
end

function env_add!(env::Env, v::TVar, lb::ASTBase, ub::ASTBase, tag::VEnvTag)
  # poor man test for alpha conversion
  if env_conflict(env,v)
    lj_error(string("Alpha-renaming error.\nv = ",VEnv(v,lb,ub,tag),"\nenv = ", env,"\n"))
  end
  append!(env.curr, [VEnv(v, lb, ub, tag, Occurs(0,0))])
end

function env_replace!(env::Env, v::TVar, lb::ASTBase, ub::ASTBase, tag::VEnvTag, state::ST_State)
  tvi = findlast(ee -> isa(ee,VEnv) && ee.var == v, env.curr)
  if tvi == 0
    lj_error(string("Internal: env_replace on missing var.  v=",v,"\nenv= ",env,"\n"))
  else
    new_occ = increase_occ(env.curr[tvi].occ, state)
    env.curr[tvi] = VEnv(v, lb, ub, tag, new_occ)
  end
end

function env_search(env::Env, tv::TVar)
  tvi = findlast(ee -> isa(ee,VEnv) && ee.var == tv , env.curr)
  if tvi == 0
    tvi = findlast(ee -> ee.var == tv , env.past)
    if tvi == 0
      lj_error(string("Internal: type variable not in scope: ",tv,"\n env: ",env))
    end
    return env.past[tvi]
  end
  return env.curr[tvi]
end

function env_defines(env::Env, tv::TVar)
  tvi = findlast(ee -> isa(ee,VEnv) && ee.var == tv , env.curr)
  if tvi == 0
    tvi = findlast(ee -> ee.var == tv , env.past)
    if tvi == 0
      return false
    end
    return true
  end
  return true
end

function env_delete!(env::Env, tv::TVar)
  # debug_out(string("<EnvDelete>",tv))
  assert(env.curr[end].var == tv)
  dv = pop!(env.curr)
  append!(env.past, [dv])
  #
  #
  # for i in 1:length(env)
  #   ee = env[i]
  #   if isa(ee,VEnv)
  #     debug_out(string("<eeBefore>", env[i], "</eeBefore>"))
  #     nub = substitute(ee.ub, tv, TAny())
  #     nlb = substitute(ee.lb, tv, TUnion([]))
  #     env[i] = VEnv(ee.var, nlb, nub, ee.tag)
  #     debug_out(string("<eeAfter>", env[i], "</eeAfter>"))
  #
  #   end
  # end
  # debug_out("</EnvDelete>")
  # filter!(ee -> !(isa(ee,VEnv)) || ee.var != tv, env)
end

function env_delete!(env::Env, ::EnvBarrier)
  assert(isa(env.curr[end], EnvBarrier))
  pop!(env.curr)
end
