# LambdaJulia

LambdaJulia is a project that pursue understanding of Julia type system and,
especially, its subtyping and miltiple dispatch mechanisms. It consists of 
several subprojects:

* *julia-ott* -- a formalization of Julia's type system.

* *type_validator* -- a mechanization of *julia-ott*.

* *julia-log* -- a modification of Julia's VM aimed to collect information about
    the usage of the most interesting part of a Julia language engine: 
    subtype/specificity checks. This effort should, among other, help to verify
    correctness of *juila-ott*.

* *julia-stats* -- a subproject for collecting statistics on actual Julia
    codebase: a huge collection of GiHub projects using Julia.

## julia-log

Modified Julia's VM lives in `julia-log*` (possible suffix point to the Julia version it is based on) subdirectory. There are three tested scenarios for using it.

1. Log one Julia program of your choice:

        python3 src/log_program.py <your-program>

    This will create `log_subt.txt` and `log_spec.txt` in current directory with logs for subtype / specificity checks during the execution of your program.

2. Logging a Julia project:

        python3 src/collect_logs.py <project-name>

    We use two template programs to collect logs froma given project, named
    `add-PROJ` and `test-PROJ`, where `PROJ` is a name of a project. Those programs
    look like this:

    * `using PROJ`
    * `Pkg.test("PROJ")`
    
    Output is written in the current directory, creating subdirectory named `PROJ`.

3. Batch logging of top Julia projects.
    
        python3 src/collect_logs.py
    
    Some 100 projects to log. [Have to be described in more detail]
    
    Output is written to the `logging/100pkgs` directory.

## *type_validator*

`type_validator` code is meant to be a mechanization of the rules for Julia type 
system as formalized by *julia-ott*.

### Contents

The meaning of the main files is briefly summarized below.

* `type_validator.jl` -- general entry point for our framework. Loads two main utilities: `lj_subtype` and `lj_typeof` -- they are Lambda-Julia's implementation of Julia's `issubtype` and `typeof`.

* `AST.jl` -- types for AST nodes: we convert Julia's AST into those using utilities from `parsing.jl`.

* `decls_dump.jl` -- a code to dump fake (empty) declarations of types from `Base` library (and probably beyond). Those are needed in `lj_subtype`/`lj_typeof` algorithms.

* `decls_base_inferred.jl` -- a collection of fake type declaration precisely from the `Base` linrary (the output of `dump_decls`).

* `test_*` -- a collection of unit tests (via Julia's `Base.Test`). Entry point which runs everything: `test_all.jl`.

* `subtype_xml.jl` and `typeof.jl` -- core files which contain an implementation for `lj_subtype`/`lj_typeof`.

## Where *type_validator* and *julia-log* meet: `process_logs`

`process_logs` is a series of scripts to test `type_validator` against the real
results of the Julia's subtype checks preserved by *julia-log*.

Just call:

    julia process_logs_entry.jl /full/path/to/log/file

And you will get a directory with the results of validation of *julia-ott* as
implemented in *type_validator* against given log file. The directory will have
the same base name and lie along the log file.

### Automation for `process_logs`

You can run `process_logs` either 1) against a list of "known", hard coded 10 projects (where 10 is in base unknown, used to be ten), or 2) from the collection of logged projects contained in a given directory (command-line argument). Ten projects are stored in archive `logging/10pkgs.tar.bz2` and meant to be unpacked into
`logging/10pkgs`. 

For first option just run:

    python3 src/process_logs_10.py

**Note 3** at the end of `python`.

For second, just add the name of directory containg "projects directories" (format of a "project directory" is determined by the `collect_logs` utility).

