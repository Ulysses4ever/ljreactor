include("typeof.jl")
include("subtype_xml.jl")
include("parsing.jl")

########################  Pseudo Base type declarations     ######################
# We need those to bootstrap our framework.
# Please, check out "aux.jl" to set `decls_mode` switch.
include("aux.jl")

#println("Current base_types_decls: $(base_types_decls)")
#dump(base_types_decls)

make_tydecl_dict(tds) = 
    Dict(zip( map(td-> "$(td.qual)::$(td.name)", tds)
            , tds))

preparsed_tydecls = 
    vcat(
        map( lj_parse_tydecl_json
           , JSON.parsefile(base_types_decls_fname)),
        map( lj_parse_tydecl_simple
           , readlines(decls_extra_fname)))

parsed_base_ty_decls = make_tydecl_dict(preparsed_tydecls)
    

#####################     Entry points to the library     ######################

function lj_typeof(t :: String, tds :: Vector{String} = String[])
    #println("\n",typeof(parsed_base_ty_decls))
    tds1 = merge(parsed_base_ty_decls, 
                 make_tydecl_dict(isempty(tds) ? 
                                     TyDecl[] : map(lj_parse_tydecl_simple, tds)))
    #println("\n",typeof(tds1))
    tp = lj_parse_type(t)
    t1 = simplify(tp, tds1, Env([], []))
    lj_typeof(t1, tds1, Env([], []))
end

function lj_subtype(t1 :: ASTBase, t2 :: ASTBase, tds :: TyDeclCol = TyDeclCol([]))
  global ST_initial_state
  lj_subtype(t1, t2, tds, Env([],[]), ST_initial_state)
end

function lj_subtype(t1 :: String, t2 :: String, tds :: Vector{String} = String[])
    if f_debug
      @printf "\n<?xml version=\"1.0\"?>\n"
    end
    tds1 = merge(parsed_base_ty_decls, 
             make_tydecl_dict(isempty(tds) ? 
                                 TyDecl[] : map(lj_parse_tydecl_simple, tds)))
    lj_subtype(lj_parse_type(t1),
               lj_parse_type(t2),
               tds1)
end

