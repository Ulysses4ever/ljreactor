abstract type A end

struct B <: A end
struct C <: A end

function cartesian(l)
  if isempty(l)
    return [[]]
  else
    x = l[end]
    xs = l[1:end-1]
    rest = cartesian(xs)
    return vcat(map(i -> (map(rs -> push!(copy(rs),i), rest)), x)...)
  end
end

# function cartesian(x :: Vector{Vector{A}}) #
#     #println("cartesian for ", x)
#     res = Vector{Vector{A}}()
#     if length(x) == 0
#         return res
#     elseif length(x) == 1
#         return map(z -> [z], x[1])
#     end
#     for i in 1:length(x[1])
#         #println("pick: ", x[1][i])
#         rec = cartesian(x[2:end])
#         #println("rec:  ", rec)
#         res = vcat(res, map(z -> vcat([x[1][i]], z), rec))
#         #println("done: ", res)
#     end
#     res
# end


@printf "%s\n" (cartesian([[1,2],[3,4],[5,6]]))
