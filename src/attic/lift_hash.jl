
# get rid of xml markup
function show(io::IO, t::TWhere)
  print(io,t.t)
  print(io," where ")
  if t.ub != TAny()
    if t.lb != EmptyUnion
      print(io, t.lb)
      print(io, " <: ")   #&lt;:
    end
    print(io,t.tvar)
    print(io, " <: ")     #&lt;:
    print(io, t.ub)
  else
    print(io,t.tvar)
    if t.lb != EmptyUnion
      print(io, " >: ")   #:gt;
      print(io, t.lb)
    end
  end
end

# lift TNames with hashes to Function
# TODO: what if the TName with a # is on the lhs of the TApp?

function lift_hash_to_function(t::ASTBase)
  return t
end

function lift_hash_to_function(t::TName)
  if contains(string(t.name), "HHHH")
    return TName("Function")
  else
    return t
  end
end

function lift_hash_to_function(t::TUnion)
  TUnion(map(lift_hash_to_function,t.ts))
end

function lift_hash_to_function(t::TTuple)
  TTuple(map(lift_hash_to_function,t.ts))
end

function lift_hash_to_function(t::TApp)
  TApp(lift_hash_to_function(t.t), map(lift_hash_to_function,t.ts))
end

function lift_hash_to_function(t::TWhere)
  return TWhere(lift_hash_to_function(t.t), t.tvar,
                lift_hash_to_function(t.lb),
                lift_hash_to_function(t.ub))
end

function lift_hash_to_function(t::TUnionAll)
  return TUnionAll(lift_hash_to_function(t.t))
end

function lift_hash_to_function(t::TType)
  return TType(lift_hash_to_function(t.t))
end

function get_rid_of_hash(s::String)
  s = replace(s,'#',"HHHH")
  return string(lift_hash_to_function(lj_parse_type(s)))
end

### all the code above is likely to be dead due to new lj_ behaviour
