include("AST.jl")

#####################       Subtyping                 #####################

# var environment for subtyping

abstract type VEnvTag end
struct TagLeft <: VEnvTag end
struct TagRight <: VEnvTag end

struct VEnv
    var :: TVar
    lb  :: ASTBase
    ub  :: ASTBase
    tag :: VEnvTag
end

function show(io::IO, venv::VEnv)
  print(io,"[",venv.var," ^",venv.ub," _",venv.lb," ",venv.tag,"] ")
end

function show(io::IO, ::TagLeft)
  print(io,"L")
end

function show(io::IO, ::TagRight)
  print(io,"R")
end

function show(io::IO, venv::Vector{VEnv})
  map(v -> print(io,v), venv)
end

struct SR
    sub :: Bool
    venv :: Vector{VEnv}
end

function show(io::IO, sr::SR)
  print(io,"= ",sr.sub,"\n= ",sr.venv)
end

### Debug

debug_pp_indent = 2

f_debug = false
f_debug_int = f_debug

function debug_indent_more()
  global debug_pp_indent
  debug_pp_indent = debug_pp_indent + 4
end

function debug_indent_less()
  global debug_pp_indent
  debug_pp_indent = debug_pp_indent - 4
end

function debug_out(s::String)
  global f_debug_int
  if f_debug_int
    global debug_pp_indent
    for i=1:debug_pp_indent
      @printf "."
    end
    @printf "%s\n" s
  end
end

function debug_subtype(rn::String, t1::ASTBase, t2::ASTBase, venv ::Vector{VEnv})
  global f_debug_int
  if f_debug_int
    debug_out(string("## ",rn))
    debug_out(string("   t1: ",t1))
    debug_out(string("   t2: ",t2))
    debug_out(string(" venv: ",venv))
  end
end

function debug_subtype_result(sr::SR)
  global f_debug_int
  if f_debug_int
    debug_out(string("## ",sr.sub))
    debug_out(string("## ",sr.venv))
  end
end

### consistency check for venv

function consistent_venv(venv::Vector{VEnv}, tds::Vector{TyDecl})
  for v in venv
    venv_work = copy(venv)
    debug_out(string("Checking: ",v))
    global f_debug_int
    global f_debug
    f_debug_int = false
    if !(lj_subtype(v.lb,v.ub, tds, venv_work).sub)
      if f_debug
        f_debug_int = true
      end
      debug_out("  failed.")
      return false
    end
    if f_debug
      f_debug_int = true
    end
    debug_out("  ok.")
  end
  return true
end



### simple simplification of unions (taken from simple_join in subtype.c)

function in_union(t1::ASTBase, t2::ASTBase)
  if t1 == t2
    return true
  elseif !isa(t1,TUnion)
    return false
  else
    return any(t -> in_union(t,t2), t1.ts)
  end
end


function simple_join(t1::ASTBase, t2::ASTBase)
  if t1 == TUnion([]) || t2 == TAny || t1 == t2
    return t2
  elseif t2 == TUnion([]) || t1 == TAny
    return t1
  # elseif isa(t1,TUnion) && in_union(t1,t2)
  #   return t2
  # elseif isa(t2,TUnion) && in_union(t2,t1)
  #   return t1
  else
    return TUnion([t1, t2])
  end
end
#
#
#     if (!(jl_is_type(a) || jl_is_typevar(a)) || !(jl_is_type(b) || jl_is_typevar(b)))
#         return (jl_value_t*)jl_any_type;
#     if (jl_is_uniontype(a) && in_union(a, b))
#         return a;
#     if (jl_is_uniontype(b) && in_union(b, a))
#         return b;
#     if (jl_is_kind(a) && jl_is_type_type(b) && jl_typeof(jl_tparam0(b)) == a)
#         return a;
#     if (jl_is_kind(b) && jl_is_type_type(a) && jl_typeof(jl_tparam0(a)) == b)
#         return b;
#     if (!jl_has_free_typevars(a) && !jl_has_free_typevars(b)) {
#         if (jl_subtype(a, b)) return b;
#         if (jl_subtype(b, a)) return a;
#     }
#     return jl_new_struct(jl_uniontype_type, a, b);
# }



#####################  Subtype algorithm ####################

# unless two types are one subtype of the other, they are not.

function lj_subtype(t1::ASTBase, t2::ASTBase, ::Vector{TyDecl}, venv::Vector{VEnv})
    debug_subtype("ASTBase, ASTBase",t1,t2,venv)
    # last hope, reflexivity
    sr = SR(t1 == t2, venv)
    debug_subtype_result(sr)
    return sr
end

# any is supertype of all types

function lj_subtype_Any_Right(t1::ASTBase, t2::TAny, tds::Vector{TyDecl}, venv::Vector{VEnv})
    debug_subtype("ASTBase, TAny",t1,t2,venv)
    sr = SR(true, venv)
    debug_subtype_result(sr)
    return sr
end

function lj_subtype(t1::TUnion, t2::TAny, tds::Vector{TyDecl}, venv::Vector{VEnv})
    debug_subtype("TUnion, TAny",t1,t2,venv)
    lj_subtype_Any_Right(t1,t2,tds,venv)
end

function lj_subtype(t1::ASTBase, t2::TAny, tds::Vector{TyDecl}, venv::Vector{VEnv})
    debug_subtype("ASTBase, TAny",t1,t2,venv)
    lj_subtype_Any_Right(t1,t2,tds,venv)
end

function lj_subtype(t1::TVar, t2::TAny, tds::Vector{TyDecl}, venv::Vector{VEnv})
    debug_subtype("TVar, TAny",t1,t2,venv)
    lj_subtype_Any_Right(t1,t2,tds,venv)
end

# unions obey a forall/exist strategy

function lj_subtype(t1::TUnion, t2::TAny, tds::Vector{TyDecl}, venv::Vector{VEnv})
    debug_subtype("TUnion, TAny",t1,t2,venv)
    sr = SR(true, venv)
    debug_subtype_result(sr)
    return sr
end

function lj_subtype_Union_Left(t1::TUnion, t2:: ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
  for ti in t1.ts
    venv_saved = deepcopy(venv)
    sr = lj_subtype(ti, t2, tds, venv_saved)
    if !sr.sub
      debug_subtype_result(sr)
      return sr
    end
  end
  sr = SR(true, venv_saved)   # TODO : think about venv in this case, this is not even remotely right
  debug_subtype_result(sr)
  return sr
end

# function lj_subtype_Union_Left(t1::TUnion, t2:: ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
#   for ti in t1.ts
#     sr = lj_subtype(ti, t2, tds, venv)
#     if !sr.sub
#       debug_subtype_result(sr)
#       return sr
#     end
#     venv = sr.venv
#   end
#   sr = SR(true, venv)   # TODO : think about venv in this case, this is not even remotely right
#   debug_subtype_result(sr)
#   return sr
# end

function lj_subtype(t1::TUnion, t2::TUnion, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TUnion, TUnion",t1,t2,venv)
  lj_subtype_Union_Left(t1,t2,tds,venv)
end

function lj_subtype(t1::TUnion, t2::TVar, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TUnion, TVar",t1,t2,venv)
  lj_subtype_Union_Left(t1,t2,tds,venv)
end

function lj_subtype(t1::TUnion, t2::TWhere, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TUnion, TWhere",t1,t2,venv)
  lj_subtype_Union_Left(t1,t2,tds,venv)
end

function lj_subtype(t1::TUnion, t2::ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TUnion, ASTBase",t1,t2,venv)
  lj_subtype_Union_Left(t1,t2,tds,venv)
end

function lj_subtype_Union_Right(t1::ASTBase, t2::TUnion, tds::Vector{TyDecl}, venv::Vector{VEnv})
  venv_saved = deepcopy(venv)
  for ti in t2.ts
    sr = lj_subtype(t1, ti, tds, venv_saved)
    if sr.sub
      debug_subtype_result(sr)
      return sr
    end
    venv_saved = deepcopy(venv)
  end
  sr = SR(false, venv_saved)
  debug_subtype_result(sr)
  return sr
end


function lj_subtype(t1::TName, t2::TUnion, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TName, TUnion",t1,t2,venv)
  lj_subtype_Union_Right(t1, t2, tds, venv)
end

function lj_subtype(t1::ASTBase, t2::TUnion, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("ASTBase, TUnion",t1,t2,venv)
  lj_subtype_Union_Right(t1, t2, tds, venv)
end


# var_left

function lj_subtype_Var_Left(t1::TVar, t2::ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
  vi1 = findlast(v -> v.var == t1 , venv)
  if vi1 == 0
    error(string("type variable not in scope: ",t1,"\n venv: ",venv))
  end
  v1 = venv[vi1]

#  @printf "*** %s\n"  v1

  if v1.tag == TagLeft()
      sr = lj_subtype(v1.ub, t2, tds, venv)
  else
      sr = lj_subtype(v1.lb, t2, tds, venv)
      # FZN: a priori we need to compute an intersection here.
      # Jeff says this is always the previous type, so let's assert this.
      # debug_out(string("\n@@ t = ", t2, "\n@@ u = ", v1.ub, "\n@@ check that meet(t,u) = t\n"))
      filter!(v -> v.var != t1, sr.venv)
      append!(sr.venv, [VEnv(t1, v1.lb, t2, TagRight())])
  end
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TVar, t2::TUnion, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TVar, TUnion",t1,t2,venv)
  lj_subtype_Var_Left(t1,t2,tds,venv)
end

function lj_subtype(t1::TVar, t2::ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TVar, ASTBase",t1,t2,venv)
  lj_subtype_Var_Left(t1,t2,tds,venv)
end

# var_right

function lj_subtype_Var_Right(t1::ASTBase, t2::TVar, tds::Vector{TyDecl}, venv::Vector{VEnv})
  v2 = venv[findlast(v -> v.var == t2, venv)]

  if v2.tag == TagLeft()
    sr = lj_subtype(t1, v2.lb, tds, venv)
  else
    sr = lj_subtype(t1, v2.ub, tds, venv)
    filter!(v -> v.var != t2, sr.venv)
    append!(sr.venv, [VEnv(t2, simple_join(v2.lb, t1), v2.ub, TagRight())])
  end
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::ASTBase, t2::TVar, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("ASTBase, TVar",t1,t2,venv)
  lj_subtype_Var_Right(t1,t2,tds,venv)
end

# special case when comparing two vars
# TODO: think when one of the bounds is itself a variable

function lj_subtype(t1::TVar, t2::TVar, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TVar, TVar",t1,t2,venv)

  if t1 == t2
    sr = SR(true, venv)
    debug_subtype_result(sr)
    return sr
  end

  v1 = venv[findlast(v -> v.var == t1, venv)]
  v2 = venv[findlast(v -> v.var == t2, venv)]

  if v2.tag != TagRight()
    sr = lj_subtype_Var_Left(t1,t2,tds,venv)
  elseif v1.tag == TagLeft()
    if lj_subtype(v1.ub, v2.ub, tds, venv).sub
      filter!(v -> v.var != t2, venv)
#      append!(venv, [VEnv(t2, TUnion([v2.lb, t1]), v2.ub, TagRight())])
      append!(venv, [VEnv(t2, simple_join(v2.lb, t1), v2.ub, TagRight())])

      sr = SR(true, venv)
    else
      sr = SR(false, venv)
    end
  elseif v1.tag == TagRight()
    if lj_subtype(v1.lb, v2.ub, tds, venv).sub
      filter!(v -> v.var != t2, venv)
      append!(venv, [VEnv(t2, simple_join(v2.lb, t1), v2.ub, TagRight())])

      sr = SR(true, venv)
    else
      sr = SR(false, venv)
    end
  end
  debug_subtype_result(sr)
  return sr
end

# unionall

# FZ: this is needed to disambiguate dispatch, as the TWhere, ASTBase case
function lj_subtype_Where_Left(t1::TWhere, t2::ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
  # poor man test for alpha conversion
  if findfirst(v -> v.var == t1.tvar, venv) != 0
    error(string("Alpha-renaming error.\nt1 = ",t1,"\nt2 = ",t2,"\nvenv = ", venv,"\n"))
  end
  append!(venv, [VEnv(t1.tvar, t1.lb, t1.ub, TagLeft())])

  sr = lj_subtype(t1.t, t2, tds, venv)

  if sr.sub == true && !(consistent_venv(sr.venv,tds))
    debug_out(string("** venv not consistent: ",venv))
    sr = SR(false,sr.venv)
    debug_subtype_result(sr)
    return sr
  end

  filter!(v -> v.var != t1.tvar, sr.venv)
  sr = SR(sr.sub, sr.venv)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TWhere, t2::TWhere, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TWhere, TWhere",t1,t2,venv)
  lj_subtype_Where_Left(t1,t2,tds,venv)
end

function lj_subtype(t1::TWhere, t2::TUnion, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TWhere, TUnion",t1,t2,venv)
  lj_subtype_Where_Left(t1,t2,tds,venv)
end

# FZN: unsure about this, as it might cause var to escape their scope in constraints
function lj_subtype(t1::TWhere, t2::TVar, tds::Vector{TyDecl}, venv::Vector{VEnv})
 debug_subtype("TWhere, TVar",t1,t2,venv)
 lj_subtype_Where_Left(t1,t2,tds,venv)
 # lj_subtype_Var_Right(t1,t2,tds,venv)
end

function lj_subtype(t1::TWhere, t2::ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TWhere, ASTBase",t1,t2,venv)
  lj_subtype_Where_Left(t1,t2,tds,venv)
end

function lj_subtype_Where_right(t1::ASTBase, t2::TWhere, tds::Vector{TyDecl}, venv::Vector{VEnv})
  # poor man test for alpha conversion
  if findfirst(v -> v.var == t2.tvar, venv) != 0
    error(string("Alpha-renaming error.\nt1 = ",t1,"\nt2 = ",t2,"\nvenv = ", venv,"\n"))
  end
  append!(venv, [VEnv(t2.tvar, t2.lb, t2.ub, TagRight())])

  sr = lj_subtype(t1, t2.t, tds, venv)

  if sr.sub == true && !(consistent_venv(sr.venv,tds))
    debug_out(string("** venv not consistent: ",venv))
    sr = SR(false,sr.venv)
    debug_subtype_result(sr)
    return sr
  end

  filter!(v -> v.var != t2.tvar, sr.venv)

  sr = SR(sr.sub, sr.venv)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::ASTBase, t2::TWhere, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("ASTBase, TWhere",t1,t2,venv)
  lj_subtype_Where_right(t1,t2,tds,venv)
end

# FZN: unsure about this, as it might cause var to escape their scope in constraints
function lj_subtype(t1::TVar, t2::TWhere, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TVar, TWhere",t1,t2,venv)
  lj_subtype_Where_right(t1,t2,tds,venv)
  # lj_subtype_Var_Left(t1,t2,tds,venv)
end

# tuples are covariant

function lj_subtype(t1::TTuple, t2::TUnion, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TTuple, ASTBase",t1,t2,venv)

  t1_lift_union = lift_union(t1)
  if t1 == t1_lift_union
    lj_subtype_Union_Right(t1, t2, tds, venv)
  else
   sr = lj_subtype(t1_lift_union, t2, tds, venv)
   debug_subtype_result(sr)
   return sr
  end
end


function lj_subtype(t1::TTuple, t2::TTuple, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TTuple, TTuple",t1,t2,venv)

  if length(t1.ts) != length(t2.ts)
    SR(false, venv)
  else
    ts = [(t1.ts[i],t2.ts[i]) for i in 1:length(t1.ts)]
    venv_iter = venv
    for tp in ts
      sr = lj_subtype(tp[1], tp[2], tds, venv_iter)
      if sr.sub == false
        return SR(false, venv_iter)
      else
        venv_iter = sr.venv
      end
    end
  debug_subtype_result(sr)
  return sr
  end
end



function lj_subtype(t1::TName, t2::TName, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TName, TName",t1,t2,venv)

  if t1 == t2
    SR(true, venv)
  else
    td1i = findfirst(td -> td.name == t1.name, tds)
    if td1i == 0
     throw(ErrorException(@sprintf "type %s not defined" t1))
      return SR(false, venv)
    end
    td1 = tds[td1i]
    sr = lj_subtype(td1.super, t2, tds, venv)
    debug_subtype_result(sr)
    return sr
  end
end

### type application is invariant

function lj_subtype(t1::TApp, t2::TApp, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TApp, TApp",t1,t2,venv)

  # TODO: this is only correct if t1.t = TName
  # TODO: also, only if number of params is the same in the two applications

  debug_out("TApp, TApp: left to right")
  debug_out(string("      venv: ", venv))
  debug_indent_more()

  ts = [(t1.ts[i],t2.ts[i]) for i in 1:length(t1.ts)]
  venv_iter = venv
  res = true
  for tp in ts
    sr = lj_subtype(tp[1], tp[2], tds, venv_iter)
    res = res && sr.sub
    venv_iter = sr.venv
  end

  # ltr = all(tts -> lj_subtype(tts[1], tts[2], tds, venv).sub, t12ts)

  debug_indent_less()
  debug_out("TApp, TApp: right to left")
  debug_out(string("      venv: ", venv_iter))

  debug_indent_more()

  for tp in ts
    sr = lj_subtype(tp[2], tp[1], tds, venv_iter)
    res = res && sr.sub
    venv_iter = sr.venv
  end

#  rtl = all(tts -> lj_subtype(tts[2], tts[1], tds, venv).sub, t12ts)

  debug_indent_less()
  sr = SR(t1.t == t2.t && res, venv_iter)  # TODO : venv_iter here is likely to be incorrect
  debug_subtype_result(sr)
  return sr
end

# the degenerate case
function lj_subtype(t1::TApp, t2::TName, tds::Vector{TyDecl}, venv::Vector{VEnv})
  debug_subtype("TApp, TName",t1,t2,venv)

  sr = SR(t1.t == t2, venv)
  debug_subtype_result(sr)
  return sr
end

# function lj_subtype(t1::TName, t2::ASTBase, tds::Vector{TyDecl}, venv::Vector{VEnv})
#   FZN: I believe this is never used
#   debug_subtype("TName, ASTBase",t1,t2,venv)
#
#   if t1 == t2
#     true
#   else
#     false # FZ TODO
#   end
# end
#


#function simplify_Union(t1 :: TUnion)
#    TUnion(t1.ts)
#end
