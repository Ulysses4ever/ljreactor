#
# Run from src with python3
#
from os import system
from multiprocessing import Pool
import os
import sys
import subprocess

# Paths (trivia)
my_path = os.path.dirname(os.path.abspath(__file__))
lj_root = os.path.join(my_path, "../")
julia_log_bin =  os.path.join(lj_root, "julia-log-06rc2/julia")
uniq = os.path.join(lj_root, "src/filter_uniques.jl")
beats = os.path.join(lj_root, "src/spec-beats-subt.jl")

# Paths (control)
logging = os.path.join(lj_root, "logging/100pkgs")
prjs100_fname = "pkgs_list_ok1.txt"
pkgs_dir = "pkgs"

# Paths (more trivia)
pkgs_path = os.path.abspath(pkgs_dir)

# Other constants
clean_install = True
proc_num = 20
tmout = 60 * 40 # 40 minutes

prjs10 = [
        "ImageView.jl",
        "MXNet.jl"#, # Mocha
        "ParallelAccelerator.jl", #DSGE
        "DataFrames.jl",
        "PyCall.jl",
        "Interact.jl", # Plots
        "Bio.jl", # Escher
        "Images.jl", # DifferentialEquations
        "Optim.jl", # Optim
        "Gadfly.jl",
        "JuMP.jl",
        "Distributions.jl"
        ]

#prjs = list(set(prjs100) - set(prjs10))


def err(d, s, m):
    system("cd {0} && echo '{1} on {2}' >> error".format(d, s, m))

def task(pr):
    #create a dir
    prdir = "{0}/{1}".format(logging,pr)
    system("if [ -d {0} ]; then rm -rf {0}; fi".format(prdir))
    system("mkdir {0}".format(prdir))

    # loop over two type of programs we log for a project
    for mode in ["add", "test"]:
        #build a command to put in file to run julia-log against
        if mode == "add":
            cmd = "'using {0}'".format(pr)
        elif mode == "test":
            cmd = "'inlude({0})'".format(os.path.join(pkgs_path, "v0.6", pr, "test/runtests.jl"))

        # create a file
        cmdfname = "{0}-{1}.jl".format(mode,pr)
        system("if [ -f {0}/{1} ]; then rm {0}/{1}; fi".format(prdir,cmdfname))
        system("echo {0} > {1}/{2}".format(cmd,prdir,cmdfname))

        # how we run julia-log:
        #julia-log/julia -e 'Pkg.test("JuMP")' 2> >(julia src/filter_uniques.jl)
        cmdlog = ("cd {0} && " +
                  "{1} {2} 2> >(julia {3} {4}) " # && " +
        ).format(prdir,julia_log_bin, cmdfname, uniq, mode)
        print("run {2}-program on {0} with:\n{1}".format(pr, cmdlog, mode))
        #cmdlog1 = "sleep {0}".format(os.getppid() % 3)
        try:
            res = subprocess.run(cmdlog, shell=True, executable="/bin/bash", timeout=tmout)
            if res.returncode != 0:
                system("cd {0} && echo 'Return code for log-subprocess: {1}' >> error".format(prdir, res.returncode))
        except subprocess.TimeoutExpired:
            err(prdir, "Timeout of log-subprocess", mode)
        except Exception as e:
            err(prdir, str(e) + "\n-- from log-subprocess", mode)
        except:
            err(prdir, "Unknown error from log-subprocess", mode)

        # spec-beats-subt
        beatscmd = "cd {2} && julia {0} {1}".format(beats, mode, prdir)
        print("run beats {2}-program on {0} with:\n{1}".format(pr, beatscmd, mode))
        try:
            #x = 1
            subprocess.run(beatscmd, shell=True, executable="/bin/bash")
        except Exception as e:
            err(prdir, "Beats:\n" + str(e) + "\n", mode)
        except:
            err(prdir, "Beats, unknown", mode)

    return os.getppid()

def prepare_julia_cache(prjs100_path):
    # Purge pkgs database if `clean_install`
    if clean_install:
        system("rm -rf {0}".format(pkgs_dir))
    system("if [ ! -d {0} ]; then mkdir {0}; fi".format(pkgs_dir))

    # Install all pkgs of interest
    system("JULIA_PKGDIR={0} julia -e 'Pkg.init()'".format(pkgs_path))
    system("cat {0} | sed s/.jl//g >{1}".format(prjs100_path, os.path.join(pkgs_dir, "v0.6/REQUIRE")))
    system("JULIA_PKGDIR={0} julia -e 'Pkg.resolve()'".format(pkgs_path))

    # Install all test dependencies
    system("JULIA_PKGDIR={0} julia {1}/install_test_deps.jl".format(pkgs_path, my_path))


def get100():
    prjs100_path = os.path.join(my_path, prjs100_fname)
    prepare_julia_cache(prjs100_path)
    prjs = open(prjs100_path).read().splitlines()

    pool = Pool(processes=proc_num)
    #pool.map(task, prjs)

    pool.close()
    pool.join()

if __name__ == '__main__':
    print(pkgs_path)
    if len(sys.argv) == 1:
        # input(
        #     "You are about to start collecting logs for 100 fixed projects (top_pkgs_gen.py). " +
        #     "\n Instead, you could provide an argument to the script:\n" +
        #     "a name of a project to collect logs on.\n" +
        #     "Press Enter to continue or Ctrl+C to abort...")
        get100()
    else:
        logging="."
        task(sys.argv[1])
