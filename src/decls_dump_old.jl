using JSON

types_analysed = Dict()

### Representing type information for storage (in JSON), 
#      later recover in TyDecl

struct TyVarInfo
    name :: String # :Sym or Symbol("Sym")
    lb   :: String
    ub   :: String
end

struct TypeInfo
    attr :: Int
    name :: String
    qual :: String
    params :: Vector{TyVarInfo}
    super  :: String
    str    :: String
end

#####  Traversing Julia's AST
function get_tyvars(t)
    tyvars = []
    while isdefined(t, :body)
        push!(tyvars, t.var)
        t = t.body
    end
    tyvars
end

function get_body(t)
    while isdefined(t, :body)
        t = t.body
    end
    t
end
##### END of traversing

const attr_struct = 1
const attr_mutable_struct = 2
const attr_abstract_type = 3

function get_attr(t)
    if isleaftype(t)
        r = attr_struct
        if !isimmutable(t)
            r = attr_mutable_struct
        end
        r
    else
        attr_abstract_type
    end
end

replace_hashes(s :: String) = replace(s, '#', "HHHH")

function convert_tyvars(tvs)
    stvs = []
    for tv in tvs
        name = sprint(show, Symbol(replace_hashes(string(tv.name))))
        ub = replace_hashes(string(tv.ub))
        lb = replace_hashes(string(tv.lb))
        push!(stvs, TyVarInfo(name, lb, ub))
    end
    stvs
end

types = []

function chop_qual(s)
  # find last dot before type parameters start (`{`) -- this is border b/w
  # qualification and name
  qualname = split(s, '{')[1]  
  ps2 = split(qualname, '.')
  prefix = join(ps2[1:end-1], '.')
  name = ps2[end]
  
  if contains(prefix, "(")
    ps3 = split(prefix, '(')
    qual = join(ps3[2:end], '(')
    op = ps3[1] * "("
  else
    qual = prefix
    op= ""
  end
  name = op * name
  (name, qual)
end

function print_ty_decl(t)
    #t_params = show_tyvars(get_tyvars(t))
    s = supertype(t)
    #s_body = get_body(s)
    #println(f, "$(get_attr(t));$(t);$(t_params);$(s_body)")
    tyvars = convert_tyvars(get_tyvars(t))
    (t1,q) = chop_qual(string(t))
    tinfo = TypeInfo( get_attr(t), t1, q, tyvars
                    , replace_hashes(string(get_body(s))), string(t))
    push!(types, tinfo)
end

function print_subtypes(t)
  if !haskey(types_analysed, t)
    types_analysed[t] = true
    if t == typeof($)
        return
    end
    print_ty_decl(t)
    sts = subtypes(t)
    for i = 1:size(sts,1)
      try
        print_subtypes(sts[i])
      catch
        @printf("Warning: cannot print_subtypes of %s\n", sts[i])
      end
    end
  end  
end

# #=
if !isdefined(:fname_decls)
    fname_decls = length(ARGS) > 1 ? ARGS[1] : "decls_base_inferred.json"
end
start_type = Any
open(fname_decls, "w") do f
  print_subtypes(start_type)
  JSON.print(f, types, 2)
end
# =#
