include("type_validator.jl")

using Base.Test

@testset "Tests for convert_ast              " begin
    @test convert_ast(parse("Int where T")) === TWhere(TName(:Int), TVar(:T))

    # Builtin forms
    @test lj_parse_type("Type") === TWhere(TType(TVar(:T)), TVar(:T))
    @test lj_parse_type("UnionAll") === TUnionAll(TDataType())

    # Parsing typeof
    @test lj_parse_type("typeof(Type)") === TUnionAll(TDataType())
    @test lj_parse_type("typeof(print)") == TName("typeof(print)")
    @test lj_parse_type("typeof(Base.:(+))") == TName("typeof(+)", "Base")
    @test lj_parse_type("typeof(Core._apply)") == TName("typeof(_apply)", "Core")
    
    # Parsing bounds
    @test convert_ast(parse("T where T <: Int")) ===
      TWhere(TVar(:T), TVar(:T), EmptyUnion, TName(:Int))
    @test convert_ast(parse("T where T >: Int")) ===
      TWhere(TVar(:T), TVar(:T), TName(:Int), TAny())
    @test convert_ast(parse("T where Int <: T <: String")) ===
      TWhere(TVar(:T), TVar(:T), TName(:Int), TName(:String))

    # Parsing numbers
    @test convert_ast(parse("Array{Int,1}")) ==
      TApp(TName(:Array), [TName(:Int), TInt(1)])

    # Vector-Array translation
    @test convert_ast(parse("Vector{Int}")) ==
      TApp(TName(:Array), [TName(:Int), TInt(1)])
end

@testset "Tests for convert_tydecl           " begin
    # Abstract types
    td1 = convert_tydecl(parse("abstract type My end"))
    @test td1.name == :My && td1.super == TAny() && length(td1.params) == 0

    td1 = convert_tydecl(parse("abstract type My <: Bar end"))
    @test td1.name == :My && td1.super == TName(:Bar) && length(td1.params) == 0

    td1 = convert_tydecl(parse("abstract type My{T} end"))
    @test td1.name == :My && td1.super == TAny() && td1.params[1] == (EmptyUnion, :T, TAny())

    td1 = convert_tydecl(parse("abstract type My{T} <: Bar end"))
    @test td1.name == :My && td1.super == TName(:Bar) && td1.params[1] == (EmptyUnion, :T, TAny())

    # Bounded ty-vars
    td1 = convert_tydecl(parse("abstract type My{T <: Int} end"))
    @test td1.name == :My && td1.super == TAny() && td1.params[1] == (EmptyUnion, :T, TName(:Int))

    td1 = convert_tydecl(parse("abstract type My{T >: Int} end"))
    @test td1.name == :My && td1.super == TAny() && td1.params[1] == (TName(:Int), :T, TAny())

    td1 = convert_tydecl(parse("abstract type My{String <: T <: Int} end"))
    @test td1.name == :My && td1.super == TAny() && td1.params[1] == (TName(:String), :T, TName(:Int))
    # we don't parse bounds on params of supertypes for nowe
    #@test convert_tydecl(parse("abstract type Bar{Tv} <: Foo{Tv<:Int} end")) == TyDecl(:Bar, Tuple{ASTBase,Symbol,ASTBase}[(TUnion(ASTBase[]), :Tv, TAny())], TApp(TName(:Foo), ASTBase[TVar(:Tv, TUnion(ASTBase[]), TName(:Int))]), Abstract())


    # Concrete types
    td1 = convert_tydecl(parse("struct My end"))
    @test td1.name == :My && td1.super == TAny() && length(td1.params) == 0
    
    td1 = convert_tydecl(parse("struct My <: Bar end"))
    @test td1.name == :My && td1.super == TName(:Bar) && length(td1.params) == 0
    
    td1 = convert_tydecl(parse("struct My{T} end"))
    @test td1.name == :My && td1.super == TAny() && td1.params[1] == (EmptyUnion, :T, TAny())
    
    td1 = convert_tydecl(parse("struct My{T} <: Bar end"))
    @test td1.name == :My && td1.super == TName(:Bar) && td1.params[1] == (EmptyUnion, :T, TAny())
end

