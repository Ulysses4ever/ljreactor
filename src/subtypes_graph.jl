types_analysed = Dict()

function print_subtypes(f, t)
  @printf("- %s\n", t)

  if !haskey(types_analysed, t)  && t != Function
    types_analysed[t] = true
  
    sts = subtypes(t)
    for i = 1:size(sts,1)
      @printf(f, " \"%s\" -> \"%s\"\n", sts[i], t)
      try
        print_subtypes(f, sts[i])
      catch
        @printf("Warning: cannot print_subtypes of %s\n", sts[i])
      end
    end
  end  
end

open("subtypes.dot", "w") do f
  @printf(f, "digraph G {\n")

  start_type = Any

  print_subtypes(f, start_type)

  @printf(f, "}\n")

end

