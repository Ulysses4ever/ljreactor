from os import system
from multiprocessing import Pool
import os
import sys
import subprocess

lj_root = os.path.dirname(os.path.abspath(__file__)) + "/../"
lj_src = lj_root + "src/"
lj_proc_ent = lj_src + "process_logs_entry.jl"
root_10pkgs = lj_root + "logging/10pkgs/"

prjs = ["ImageView",
        "MXNet"#, # Mocha
        "ParallelAccelerator", #DSGE
        "DataFrames",
        "PyCall",
        "Interact", # Plots
        #"Bio", # Escher
        "Images", # DifferentialEquations
        "Optim", # Optim
        "Gadfly",
        ]

tmout= 60 * 90

def task(pr):
    for mode in ["add", "test"]:

        cmdproc = 'julia {} {}/{}/{}-log_subt.txt'.format(lj_proc_ent,root_10pkgs,pr,mode)
        print('run proc-program on {} with:\n{}'.format(pr, cmdproc))
        subprocess.run(cmdproc, shell=True, executable="/bin/bash"
            , timeout=tmout)

    return 0 #os.getppid()

def job():
    pool = Pool(processes=20)
    pool.map(task, prjs)
    pool.close()
    pool.join()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        prjs = os.listdir(sys.argv[1])
        root_10pkgs = sys.argv[1]
    job()

