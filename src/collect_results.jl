#
# Run at the level where project directories are stored
#
using JSON

# Get project label like GreatProj-using or MyProj-test
# from a string like: ./GreatProj/add_log-subt/results.json
# (output of the find caommand -- see main function)
function get_proj_label(r :: String)
    ss = split(r, "/")
    pr = ss[2] * (startswith(ss[3], "add")
                  ? "-using" : "-test")
end

############  Formatting utilities
sep = "|"
bsep = "||"

bpad(s,n,m) = lpad("",n) * "$(s)" * lpad("",m)
function pwc(s, w)
    sw = w - length("$(s)")
    bpad(s, div(sw,2), sw - div(sw,2))
end
pwr(s, w) = lpad(s, w - 1) * " "
pwcb(s,w) = sep * pwc(s,w) * sep
pwcbl(s,w) = sep * pwc(s,w)
pwcbbl(s,w) = bsep * pwc(s,w)
pwcbr(s,w) = pwc(s,w) * sep
pwcbbr(s,w) = pwc(s,w) * bsep
pwrb(s,w) = sep * pwr(s,w) * sep
pwrbl(s,w) = sep * pwr(s,w)
pwrbbl(s,w) = bsep * pwr(s,w)
pwrbr(s,w) = pwr(s,w) * sep
pwrbbr(s,w) = pwr(s,w) * bsep

prw = 30
nw = 8

##########  Main printing functions

function print_head()
    colw = 26
    d = (s,n=colw) -> pwcbbr(s,n)
    println(d("PROJECT", prw), d("PARSER"), d("TYPEOF",colw+nw+1), d("SUBTYPE"))
    d = s -> pwcbl(s,nw)
    b = s -> pwcbbl(s,nw)
    println(pwr("",prw),
            b(" #types "), d(" unsupp "), d(" not WF "), 
            b(" #tests "), d(" passed "), d(" fail "), d(" ANY "), 
            b(" #tests "), d(" passed "), d(" fail "), bsep)
    println(lpad("",prw + 3*colw + nw + 7,'-') * bsep)
end

function print_proj(pr, json)
    d = s -> pwrbl(s,nw)
    b = s -> pwrbbl(s,nw)
    p = json["parser"]
    t = json["typeof"]
    s = json["subtype"]
    println(rpad(pr,prw)
           #                     Parser
           , b(p["cnt"])                        # #types
           , d(p["varg"] + p["tt"])             # unsupp
           , d(p["freevar"] + p["exc"] + t["nnf"] + t["exc"] + p["logfail"] 
                + p["undersc"])                 # not WF
           #                     Typeof
           , b(t["cnt"] - t["nnf"] - t["exc"])  # #tests
           , d(t["pos"])                        # passed
           , d(t["neg"])                        # fail
           , d(t["capany"])                     # ANY
           #                     Subtype
           , b(s["cnt"] - s["exc"])             # #tests
           , d(s["pos"])                        # passed
           , d(s["neg"]), bsep                  # fail
          )
end

function main()
    rs = readlines(pipeline(`find -name results.json`, `sort`))
    print_head()
    for r in rs
        pr = get_proj_label(r)
        print_proj(pr, JSON.parsefile(r))
    end
    println()
end

main()
