include("AST.jl")
include("env.jl")
include("typeof.jl")

#####################       Subtyping                 #####################



### output of the algorithm

struct SR
    sub :: Bool
    env :: Env
end

function show(io::IO, sr::SR)
  print(io,"= ",sr.sub,"\n= ",sr.env)
end

### Debug

f_debug = false
f_debug_count = 1

function debug_out(s::String)
  global f_debug_count
  if f_debug
    f_debug_count = f_debug_count + 1
    if f_debug_count > 1000
      lj_error("POOR MAN INFINITE LOOP DETECTION")
    end
    @printf "%s\n" s
  end
end

function debug_subtype(rn::String, t1::ASTBase, t2::ASTBase, env::Env)
  global f_debug_count
  f_debug_count = f_debug_count + 1
  if f_debug
    debug_out(string("<rule id=\"",rn,"\">"))
    debug_out(string("<t1>",t1,"</t1>"))
    debug_out(string("<t2>",t2,"</t2>"))
    debug_out(string("<env>",env,"</env>"))
    #debug_out(string("<envFV>",free_variables(env),"</envFV>"))
  end
end

function debug_subtype_result(sr::SR)
  if f_debug
    debug_out(string("<sub>",sr.sub,"</sub>"))
    debug_out(string("<env>",sr.env,"</env>"))
    debug_out(string("</rule>"))
  end
end


### counting of occurrences for the diagonal rule

### FZN bogus, this cannot be done statically

function count_occurs(v::TVar, t::ASTBase, in_cov::Bool, in_inv::Bool)
  return Occurs(0,0)
end

function count_occurs(v::TVar, t::TVar, in_cov::Bool, in_inv::Bool)
  if v == t
    if in_inv
      return Occurs(0,1)
    elseif in_cov
      return Occurs(1,0)
    end
  end
  return Occurs(0,0)
end

function count_occurs(v::TVar, t::TTuple, in_cov::Bool, in_inv::Bool)
  sum(map(t1 -> count_occurs(v,t1,true,in_inv), t.ts))
end

function count_occurs(v::TVar, t::TUnion, in_cov::Bool, in_inv::Bool)
  sum(map(t1 -> count_occurs(v,t1,in_cov,in_inv), t.ts))
end

function count_occurs(v::TVar, t::TApp, in_cov::Bool, in_inv::Bool)
  count_occurs(v,t.t,in_cov,in_inv) + sum(map(t1 -> count_occurs(v,t1,in_cov,true), t.ts))
end

function count_occurs(v::TVar, t::TType, in_cov::Bool, in_inv::Bool)
  return count_occurs(v, t.t, in_cov, true)
end

function count_occurs(v::TVar, t::ASTBase)
  return count_occurs(v,t,false,false)
end

### consistency check for Env

function consistent_env_var(v::TVar, env::Env, tds::TyDeclCol, state::ST_State)
  # TODO: check with Jeff how they avoid recursive consistency checks.
  if state.in_consistency_check
    return true
  end
  new_state = state_set_in_consistency_check(state)

  vi = findlast(ee -> isa(ee,VEnv) && ee.var == v, env.curr)
  if vi == 0
    lj_error(string("Internal: consistent_env on missing var.  v=",v,"\nenv= ",env,"\n"))
  end
  ve = env.curr[vi]

  env_work = Env(filter!(ee -> !(isa(ee,EnvBarrier)), flatten(vcat(env.curr, env.past))), [])
  env_work = deepcopy(env_work)
  debug_out(string("<ConsistencyCheck>\n<v>",ve,"</v>"))
  if !(lj_subtype(ve.lb, ve.ub, tds, env_work, new_state).sub)
    debug_out("<out>Failed.</out>\n</ConsistencyCheck>")
      return false
    end
  # diagonal check
  if ve.occ.cov >= 2 && ve.occ.inv == 0 && ve.tag == TagRight()
    if !(is_concrete(ve.lb, tds))
      debug_out("<diagonal>false</diagonal>\n<out>Failure.</out>\n</ConsistencyCheck>")
      return false
    else
      debug_out("<diagonal>true</diagonal>")
    end
  end
  debug_out("<out>Success.</out>\n</ConsistencyCheck>")

  return true
end

function consistent_env(env::Env, tds::TyDeclCol, state::ST_State)
  vl = flatten(vcat(map(ee -> ee.var ,  filter(ee -> isa(ee,VEnv), env.curr))))
  return all(v -> consistent_env_var(v, env, tds, state), vl)
end    

function sr_consistent_env(sr, tds, state)
  if sr.sub == true && !(consistent_env(sr.env, tds, state))
    return SR(false, sr.env)
  else
    return sr  
  end
end

### simple simplification of unions (taken from simple_join in subtype.c)

function in_union(t1::ASTBase, t2::ASTBase)
  if t1 == t2
    return true
  elseif !isa(t1,TUnion)
    return false
  else
    return any(t -> in_union(t,t2), t1.ts)
  end
end

function simple_join(t1::ASTBase, t2::ASTBase)
  if t1 == TUnion([]) || t2 == TAny || t1 == t2
    return t2
  elseif t2 == TUnion([]) || t1 == TAny
    return t1
  # elseif isa(t1,TUnion) && in_union(t1,t2)
  #   return t2
  # elseif isa(t2,TUnion) && in_union(t2,t1)
  #   return t1
  else
    return TUnion([t1, t2])
  end
end
#
#
#     if (!(jl_is_type(a) || jl_is_typevar(a)) || !(jl_is_type(b) || jl_is_typevar(b)))
#         return (jl_value_t*)jl_any_type;
#     if (jl_is_uniontype(a) && in_union(a, b))
#         return a;
#     if (jl_is_uniontype(b) && in_union(b, a))
#         return b;
#     if (jl_is_kind(a) && jl_is_type_type(b) && jl_typeof(jl_tparam0(b)) == a)
#         return a;
#     if (jl_is_kind(b) && jl_is_type_type(a) && jl_typeof(jl_tparam0(a)) == b)
#         return b;
#     if (!jl_has_free_typevars(a) && !jl_has_free_typevars(b)) {
#         if (jl_subtype(a, b)) return b;
#         if (jl_subtype(b, a)) return a;
#     }
#     return jl_new_struct(jl_uniontype_type, a, b);
# }



#####################  Subtype algorithm ####################

# unless two types are one subtype of the other, they are not.

function lj_subtype(t1::ASTBase, t2::ASTBase, ::TyDeclCol, env::Env, state::ST_State)
    debug_subtype("ASTBase, ASTBase",t1,t2,env)
    # last hope, reflexivity
    sr = SR(t1 == t2, env)
    debug_subtype_result(sr)
    return sr
end

# any is supertype of all types

function lj_subtype_Any_Right(t1::ASTBase, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
    sr = SR(true, env)
    debug_subtype_result(sr)
    return sr
end

function lj_subtype(t1::TUnion, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
    debug_subtype("TUnion, TAny",t1,t2,env)
    lj_subtype_Any_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TType, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
    debug_subtype("TType, TAny",t1,t2,env)
    lj_subtype_Any_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TWhere, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
    debug_subtype("TWhere, TAny",t1,t2,env)
    lj_subtype_Any_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::ASTBase, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
    debug_subtype("ASTBase, TAny",t1,t2,env)
    lj_subtype_Any_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TVar, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
    debug_subtype("TVar, TAny",t1,t2,env)
    lj_subtype_Any_Right(t1,t2,tds,env,state)
end

# unions obey a forall/exist strategy

function lj_subtype(t1::TUnion, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
    debug_subtype("TUnion, TAny",t1,t2,env)
    sr = SR(true, env)
    debug_subtype_result(sr)
    return sr
end

function env_merge_barrier(env_new::Env, env_old::Env)
  # TODO: think about merging env.past...  is it always the same?
  bi_new = findlast(ee -> isa(ee,EnvBarrier), env_new.curr)
  bi_old = findlast(ee -> isa(ee,EnvBarrier), env_old.curr)
  assert(bi_new == bi_old)
  env = Env(append!(env_new.curr[1:bi_new], env_old.curr[bi_new+1:end]), env_old.past)
  return env
end

function lj_subtype_Union_Left(t1::TUnion, t2:: ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  for ti in t1.ts
    env_saved = deepcopy(env)
    sr = lj_subtype(ti, t2, tds, env, state)
    if !sr.sub
      debug_subtype_result(sr)
      return sr
    end
    env = env_merge_barrier(sr.env, env_saved)
  end
  sr = SR(true, env)
  debug_subtype_result(sr)
  return sr
end

# function lj_subtype_Union_Left(t1::TUnion, t2:: ASTBase, tds::TyDeclCol, venv::Vector{VEnv})
#   for ti in t1.ts
#     sr = lj_subtype(ti, t2, tds, venv)
#     if !sr.sub
#       debug_subtype_result(sr)
#       return sr
#     end
#     venv = sr.venv
#   end
#   sr = SR(true, venv)   # TODO : think about venv in this case, this is not even remotely right
#   debug_subtype_result(sr)
#   return sr
# end

function lj_subtype(t1::TUnion, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TUnion, TUnion",t1,t2,env)
  lj_subtype_Union_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TUnion, t2::TVar, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TUnion, TVar",t1,t2,env)

  #lj_subtype_Var_Right(t1,t2,tds,env,state)
  lj_subtype_Union_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TUnion, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TUnion, TType",t1,t2,env)

  lj_subtype_Union_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TUnion, t2::TWhere, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TUnion, TWhere",t1,t2,env)
  lj_subtype_Union_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TUnion, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TUnion, ASTBase",t1,t2,env)
  lj_subtype_Union_Left(t1,t2,tds,env,state)
end

function lj_subtype_Union_Right(t1::ASTBase, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  env_saved = deepcopy(env)
  for ti in t2.ts
    sr = lj_subtype(t1,ti,tds,env_saved,state)
    if sr.sub
      debug_subtype_result(sr)
      return sr
    end
    env_saved = deepcopy(env)
  end
  sr = SR(false, env_saved)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TName, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TName, TUnion",t1,t2,env)
  lj_subtype_Union_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TType, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TType, TUnion",t1,t2,env)
  lj_subtype_Union_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TVar, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TVar, TUnion",t1,t2,env)
  # FZN: this is a hack to get the forall/exist quantification right...
  # FZN: unlikely to be correct if the Union in the v1 bound is not at top_level
  v1 = env_search(env, t1)
  if (v1.tag == TagLeft() && isa(lift_union(v1.ub), TUnion)) || (v1.tag == TagRight() && isa(lift_union(v1.lb), TUnion)) || t2 == TUnion([])
    lj_subtype_Var_Left(t1,t2,tds,env,state)
  else
    lj_subtype_Union_Right(t1,t2,tds,env,state)
  end
end

function lj_subtype(t1::ASTBase, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("ASTBase, TUnion",t1,t2,env)
  lj_subtype_Union_Right(t1, t2, tds, env, state)
end

# var_left

function lj_subtype_Var_Left(t1::TVar, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  v1 = env_search(env, t1)

  new_state = state_disable_occurrence_counting(state)

  work_env = deepcopy(env)
  
  if v1.tag == TagLeft()
    srt = lj_subtype(v1.ub, substitute(t2,t1,v1.ub), tds, substitute(work_env, t1,v1.ub), new_state)
    sr = SR(srt.sub, env)
  else
    # no need to substitute for TagRight
    # srt = lj_subtype(v1.lb, substitute(t2,t1,v1.lb), tds, work_env, new_state)
    srt = lj_subtype(v1.lb, t2, tds, work_env, new_state)

    # FZN: a priori we need to compute an intersection here.
    # Jeff says this is always the previous type, so let's assert this.
    # debug_out(string("\n@@ t = ", t2, "\n@@ u = ", v1.ub, "\n@@ check that meet(t,u) = t\n"))
    env_replace!(env, t1, v1.lb, t2, TagRight(), state)
    sr = SR(srt.sub, env)
  end
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TVar, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TVar, ASTBase",t1,t2,env)
  return lj_subtype_Var_Left(t1,t2,tds,env,state)
end

# var_right

function lj_subtype_Var_Right(t1::ASTBase, t2::TVar, tds::TyDeclCol, env::Env, state::ST_State)
  v2 = env_search(env, t2)

  new_state = state_disable_occurrence_counting(state)

  work_env = deepcopy(env)

  if v2.tag == TagLeft()
    srt = lj_subtype(substitute(t1,t2,v2.lb), v2.lb, tds, work_env, new_state)
    sr = SR(srt.sub, env)
  else
    srt = lj_subtype(substitute(t1,t2,v2.ub), v2.ub, tds, work_env, new_state)
    env_replace!(env, t2, simple_join(v2.lb, t1), v2.ub, TagRight(), state)
    sr = SR(srt.sub, env)
  end
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::ASTBase, t2::TVar, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("ASTBase, TVar",t1,t2,env)
  lj_subtype_Var_Right(t1,t2,tds,env, state)
end

function lj_subtype(t1::TType, t2::TVar, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TType, TVar",t1,t2,env)
  lj_subtype_Var_Right(t1,t2,tds,env, state)
end

# special case when comparing two vars
# TODO: think when one of the bounds is itself a variable

function outside(t1::TVar, t2::TVar, env::Env)
  tv1 = findlast(ee -> isa(ee,VEnv) && ee.var == t1, env.curr)
  tv2 = findlast(ee -> isa(ee,VEnv) && ee.var == t2, env.curr)

  if tv1 < tv2
    tvb = findlast(ee -> isa(ee,EnvBarrier), env.curr[tv1:tv2])
    if tvb == 0
      return false
    else
      return true
    end
  else
    return false
  end
end

function lj_subtype(t1::TVar, t2::TVar, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TVar, TVar",t1,t2,env)

  if t1 == t2
    sr = SR(true, env)
    debug_subtype_result(sr)
    return sr
  end

  v1 = env_search(env, t1)
  v2 = env_search(env, t2)

  new_state = state # state_disable_occurrence_counting(state)

  if (v2.tag == TagLeft() && v1.tag == TagLeft())
    return lj_subtype_Var_Left(t1,t2,tds,env,new_state)
  elseif v2.tag == TagLeft() && v1.tag == TagRight()
    debug_out(string("<Outside>",t1," - ",t2," : ",outside(t1,t2,env),"</Outside>"))
    if outside(t1,t2,env)
      env_aux = deepcopy(env)
      sr1 = lj_subtype(v2.ub,v2.lb,tds,env_aux,new_state)
      if !(sr1.sub)
        return SR(false,sr1.env)
      end
    end
    return lj_subtype_Var_Left(t1,t2,tds,env,new_state)

#     if v1.tag == TagLeft()
#       sr = lj_subtype(v1.ub, v2.lb, tds, env)
#     elseif v1.tag == TagRight()
# #      sr = lj_subtype_Var_Left(t1,t2,tds,env)
#       sr = lj_subtype(v1.lb, v2.lb, tds, env)
#       # FZN: a priori we need to compute an intersection here.
#       # Jeff says this is always the previous type, so let's assert this.
#       # debug_out(string("\n@@ t = ", t2, "\n@@ u = ", v1.ub, "\n@@ check that meet(t,u) = t\n"))
#       # env_replace!(sr.env, t1, v1.lb, v2.lb , TagRight())
#     end
  elseif v2.tag == TagRight()
    if v1.tag == TagLeft()
      if v2.ub == t1
        env_replace!(env, t2, simple_join(v2.lb, t1), v2.ub, TagRight(), new_state)
        sr = SR(true,env)
      else
        # sr = lj_subtype(v1.ub, v2.ub, tds, env, state)
        # env_replace!(sr.env, t2, simple_join(v2.lb, t1), v2.ub, TagRight(), new_state)
        env_copy = deepcopy(env)
        srt = lj_subtype(v1.ub, v2.ub, tds, substitute(env_copy,t1,v1.ub), state)
        env_replace!(env, t2, simple_join(v2.lb, t1), v2.ub, TagRight(), new_state)
        sr = SR(srt.sub, env)
      end
    elseif v1.tag == TagRight()
      env_copy = deepcopy(env)
      srt = lj_subtype(v1.lb, v2.ub, tds, env_copy, new_state)
      env_replace!(env, t2, simple_join(v2.lb, v1.lb), v2.ub, TagRight(), new_state)
      env_replace!(env, t1, v1.lb, v2.ub, TagRight(), new_state)
      sr = SR(srt.sub, env)
    end
  end
  debug_subtype_result(sr)
  return sr
end

# unionall

# FZ: this is needed to disambiguate dispatch, as the TWhere, ASTBase case
function lj_subtype_Where_Left(t1::TWhere, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  t1v = t1.tvar
  t1t = t1.t
  if env_conflict(env, t1.tvar)
    (t1v, t1t) = freshen(env, t1.tvar, t1.t)
  end
  env_add!(env, t1v, t1.lb, t1.ub, TagLeft())

  sr = lj_subtype(t1t, t2, tds, env, state)

  if sr.sub == true  && !(consistent_env_var(t1v, sr.env, tds, state))
    debug_out(string("<error>** env not consistent: ",env,"</error>"))
    sr = SR(false,sr.env)
    env_delete!(sr.env, t1v)
  
    debug_subtype_result(sr)
    return sr
  end

  env_delete!(sr.env, t1v)

  sr = SR(sr.sub, sr.env)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TWhere, t2::TWhere, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TWhere, TWhere",t1,t2,env)
  lj_subtype_Where_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TWhere, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TWhere, TWhere",t1,t2,env)
  lj_subtype_Where_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TWhere, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TWhere, TUnion",t1,t2,env)
  lj_subtype_Where_Left(t1,t2,tds,env,state)
end

# FZN: unsure about this, as it might cause var to escape their scope in constraints
function lj_subtype(t1::TWhere, t2::TVar, tds::TyDeclCol, env::Env, state::ST_State)
 debug_subtype("TWhere, TVar",t1,t2,env)
 #lj_subtype_Where_Left(t1,t2,tds,env)
 lj_subtype_Var_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TWhere, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TWhere, ASTBase",t1,t2,env)
  lj_subtype_Where_Left(t1,t2,tds,env,state)
end

function lj_subtype_Where_Right(t1::ASTBase, t2::TWhere, tds::TyDeclCol, env::Env, state::ST_State)
  t2v = t2.tvar
  t2t = t2.t
  if env_conflict(env, t2.tvar)
    (t2v, t2t) = freshen(env, t2.tvar, t2.t)
  end

  env_add!(env, t2v, t2.lb, t2.ub, TagRight())

  sr = lj_subtype(t1, t2t, tds, env, state)

  if sr.sub == true && !(consistent_env_var(t2v, sr.env, tds,state))
    debug_out(string("<error>not consistent: ",env,"</error>"))
    env_delete!(sr.env, t2v)
    sr = SR(false,sr.env)
    debug_subtype_result(sr)
    return sr
  end

  env_delete!(sr.env, t2v)

  sr = SR(sr.sub, sr.env)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::ASTBase, t2::TWhere, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("ASTBase, TWhere",t1,t2,env)
  lj_subtype_Where_Right(t1,t2,tds,env,state)
end

# FZN: unsure about this, as it might cause var to escape their scope in constraints
function lj_subtype(t1::TVar, t2::TWhere, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TVar, TWhere",t1,t2,env)
  #lj_subtype_Where_Right(t1,t2,tds,env,state)
  lj_subtype_Var_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TType, t2::TWhere, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TType, TWhere",t1,t2,env)
  lj_subtype_Where_Right(t1,t2,tds,env,state)
end

# tuples are covariant

function lj_subtype(t1::TTuple, t2::TSuperTuple, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TTuple, TSuperTuple",t1,t2,env)

  sr = SR(true, env)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TTuple, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TTuple, ASTBase",t1,t2,env)

  t1_lift_union = lift_union(t1)
  if t1 == t1_lift_union
    lj_subtype_Union_Right(t1, t2, tds, env, state)
  else
   sr = lj_subtype(t1_lift_union, t2, tds, env, state)
   debug_subtype_result(sr)
   return sr
  end
end

function lj_subtype(t1::TTuple, t2::TTuple, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TTuple, TTuple",t1,t2,env)

  t1_lift_union = lift_union(t1)
  if t1 != t1_lift_union
    sr = lj_subtype(t1_lift_union,t2,tds,env,state)
    debug_subtype_result(sr)
    return sr
  end

  # FZN: this might avoid the need for the exists backtracking
  t2_lift_union = lift_union(t2)
  if t2 != t2_lift_union
    sr = lj_subtype(t1,t2_lift_union,tds,env,state)
    debug_subtype_result(sr)
    return sr
  end
    
  new_state = state_set_covariant_position(state)

  if length(t1.ts) != length(t2.ts)
    return SR(false, env)
  else
    ts = [(t1.ts[i],t2.ts[i]) for i in 1:length(t1.ts)]
    env_iter = env
    sr = SR(true,env)
    for tp in ts
      sr = lj_subtype(tp[1], tp[2], tds, env_iter, new_state)   # FZN: diagonal check
      if sr.sub == false
        debug_subtype_result(sr)
        return sr
      else
        env_iter = sr.env
      end
    end
  debug_subtype_result(sr)
  return sr
  end
end

function lj_subtype(t1::TName, t2::TName, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TName, TName",t1,t2,env)
  if t1 == t2
    sr = SR(true, env)
  else
    td1 = lj_lookup(t1, tds)
    sr = lj_subtype(td1.super, t2, tds, env, state)
  end
  debug_subtype_result(sr)
  return sr
end

### type application is invariant

function simplify_app(t::TApp)
  # Either the callee is a name, and there is nothing to simplify
  if isa(t.t, TName)
    return (false,t)
  # Or the callee is a where, and we compute the application
  elseif isa(t.t, TWhere)
    if length(t.ts) == 0
      return (true, t.t.t)
    elseif length(t.ts) == 1
      return (true, substitute(t.t.t, t.t.tvar, t.ts[1]))
    elseif length(t.ts) > 1
      return (true, TApp(substitute(t.t.t, t.t.tvar, t.ts[1]), t.ts[2:end]))
    end
  else
    error(string("LHS of a TApp is not TName or TWhere: ", t))
  end
end

function def_from_name(t::TName, tds)
  td = lj_lookup(t, tds)
  vars = map(v -> TVar(v[2]), td.params)
  new_t = TApp(t, vars)
  for v in td.params
    new_t = TWhere(new_t,TVar(v[2]),v[1],v[3])
  end
  return new_t
end

function search_supertype(t1::TAny, t2::ASTBase, tds)
  return (false,t1,t2)
end

function search_supertype(t1::TAny, t2::TAny, tds)
  return (true,t1,t2)
end

function search_supertype(t1::TName, t2::ASTBase, tds)
  if t1 == t2
    return (true,t1,t2)
  else
    td1 = lj_lookup(t1, tds)
    # debug_out(string("<ready>", td1, "</ready>"))
    search_supertype(td1.super, t2, tds)
  end 
end

function search_supertype(t1::TApp, t2::ASTBase, tds)
  # debug_out(string("<ss>", t1 ," ",t2,"</ss>"))
  if (isa(t2, TName) && t1.t == t2) || (isa(t2,TApp) && t1.t == t2.t)
    # debug_out(string("<found>", t1 ,"</found>"))
    return (true,t1,t2)
  else
    # search for t1.t def in tds
    # parse the def...  name { args } <: name' { args' }
    # build new t1 as name' { subst xxx }
    # iterate

    td1 = lj_lookup(t1.t, tds)

    # debug_out(string("<lookup>", td1, "</lookup>"))

    td1_vars = map(t -> t[2], td1.params)
    if length(td1_vars) < length(t1.ts)
      error(string("Too many parameters: ", t1.ts," for ", td1))
    end

    nt1 = td1.super
    for (t,v) in zip(t1.ts, td1_vars)
      nt1 = substitute(nt1, TVar(v), t)
    end
    
    # debug_out(string("<ready>", nt1, "</ready>"))

    search_supertype(nt1, t2, tds)
  end
end

function invariance_check(t1::ASTBase, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  env_add!(env, EnvBarrier())

  debug_out("<LeftToRight>")
  debug_out(string("<env>", env,"</env>"))

  sr = lj_subtype(t1, t2, tds, env, state)

  debug_out(string("<sub>",sr.sub,"</sub>"))
  debug_out(string("<env>",sr.env,"</env>"))
  debug_out("</LeftToRight>")

  if sr.sub == true
    debug_out("<RightToLeft>")
    debug_out(string("<env>", sr.env,"</env>"))
    new_state = state_disable_occurrence_counting(state)

    sr = lj_subtype(t2, t1, tds, sr.env, new_state)

    debug_out(string("<sub>",sr.sub,"</sub>"))
    debug_out(string("<env>",sr.env,"</env>"))
    debug_out("</RightToLeft>")

  end

  env_delete!(sr.env, EnvBarrier())

  debug_subtype_result(sr)
  return sr
end

function invariance_check(ts1::Vector{ASTBase}, ts2::Vector{ASTBase}, tds::TyDeclCol, env::Env, state::ST_State)
  env_add!(env, EnvBarrier())

  ts = [(ts1[i],ts2[i]) for i in 1:length(ts2)]
  sr = SR(true, env)
  env_iter = env
  res = true
  new_state = state_disable_occurrence_counting(state)
  for tp in ts
    debug_out("<LeftToRight>")
    debug_out(string("<env>", env,"</env>"))
    sr = lj_subtype(tp[1], tp[2], tds, env_iter, state)
    res = res && sr.sub
    env_iter = sr.env
    debug_out(string("<sub>",sr.sub,"</sub>"))
    debug_out(string("<env>",sr.env,"</env>"))
    debug_out("</LeftToRight>")
    debug_out("<RightToLeft>")
    debug_out(string("<env>", env_iter,"</env>"))
    sr = lj_subtype(tp[2], tp[1], tds, env_iter, new_state)
    res = res && sr.sub
    env_iter = sr.env
    debug_out(string("<sub>",sr.sub,"</sub>"))
    debug_out(string("<env>",sr.env,"</env>"))
    debug_out("</RightToLeft>")
  end

  sr = SR(res, env_iter)

  env_delete!(sr.env, EnvBarrier())

  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TApp, t2::TApp, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, TApp",t1,t2,env)

  (flag1,st1) = simplify_app(t1)
  (flag2,st2) = simplify_app(t2)
  if flag1 || flag2
    debug_out(string("<simplifyApp>", st1, "</simplifyApp>"))
    debug_out(string("<simplifyApp>", st2, "</simplifyApp>"))
    sr = lj_subtype(st1, st2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  end

  (found,t1,t2) = search_supertype(t1,t2,tds)

  if !found
    sr = SR(false,env)
    debug_subtype_result(sr)
    return sr
  end

  new_state = state_set_invariant_position(state)
  return invariance_check(t1.ts, t2.ts, tds, env, new_state)
end

# the degenerate cases
function lj_subtype(t1::TApp, t2::TName, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, TName",t1,t2,env)

  (flag,st1) = simplify_app(t1)
  if flag
    debug_out(string("<simplifyApp>", st1, "</simplifyApp>"))
    sr = lj_subtype(st1, t2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  end

  (found,t1,t2) = search_supertype(t1,t2,tds)

  if !found
    sr = SR(false,env)
    debug_subtype_result(sr)
    return sr
  end

  sr = SR((isa(t1,TApp) && t1.t == t2) || (isa(t1,TName) && t1 == t2), env)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TApp, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, ASTBase",t1,t2,env)

  (flag,st1) = simplify_app(t1)
  if flag
    debug_out(string("<simplifyApp>", st1, "</simplifyApp>"))
    sr = lj_subtype(st1, t2, tds, env, state)
  else
    sr = SR(false, env)
  end
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::ASTBase, t2::TApp, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("ASTBase, TApp",t1,t2,env)

  (flag,st2) = simplify_app(t2)
  if flag
    debug_out(string("<simplifyApp>", st2, "</simplifyApp>"))
    sr = lj_subtype(t1, st2, tds, env, state)
  else

    if isa(t1,TName)
      # replace name by its definition (e.g. Pair(A,B) where A where B from Pair)
      new_t1 = def_from_name(t1, tds)
      sr = lj_subtype(new_t1, t2, tds, env, state)
    else
      sr = SR(false, env)
    end
  end
  debug_subtype_result(sr)
  return sr
end

# the TApp ambiguities

function lj_subtype(t1::TApp, t2::TWhere, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, TWhere",t1,t2,env)

  (flag,st1) = simplify_app(t1)
  if flag
    debug_out(string("<simplifyApp>", st1, "</simplifyApp>"))
    sr = lj_subtype(st1, t2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  else
    return lj_subtype_Where_Right(t1,t2,tds,env,state)
  end
end

function lj_subtype(t1::TApp, t2::TUnion, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, TUnion",t1,t2,env)

  (flag,st1) = simplify_app(t1)
  if flag
    debug_out(string("<simplifyApp>", st1, "</simplifyApp>"))
    sr = lj_subtype(st1, t2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  else
    return lj_subtype_Union_Right(t1,t2,tds,env,state)
  end
end

function lj_subtype(t1::TApp, t2::TVar, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, TVar",t1,t2,env)

  (flag,st1) = simplify_app(t1)
  if flag
    debug_out(string("<simplifyApp>", st1, "</simplifyApp>"))
    sr = lj_subtype(st1, t2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  else
    return lj_subtype_Var_Right(t1,t2,tds,env,state)
  end
end

function lj_subtype(t1::TApp, t2::TAny, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, TAny",t1,t2,env)

  sr = SR(true, env)
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TWhere, t2::TApp, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TWhere, TApp",t1,t2,env)

  (flag,st2) = simplify_app(t2)
  if flag
    debug_out(string("<simplifyApp>", st2, "</simplifyApp>"))
    sr = lj_subtype(t1, st2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  else
    return lj_subtype_Where_Left(t1,t2,tds,env,state)
  end
end

function lj_subtype(t1::TUnion, t2::TApp, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TUnion, TApp",t1,t2,env)

  (flag,st2) = simplify_app(t2)
  if flag
    debug_out(string("<simplifyApp>", st2, "</simplifyApp>"))
    sr = lj_subtype(t1, st2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  else
    return lj_subtype_Union_Left(t1,t2,tds,env,state)
  end
end

function lj_subtype(t1::TVar, t2::TApp, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TVar, TApp",t1,t2,env)

  (flag,st2) = simplify_app(t2)
  if flag
    debug_out(string("<simplifyApp>", st2, "</simplifyApp>"))
    sr = lj_subtype(t1, st2, tds, env, state)
    debug_subtype_result(sr)
    return sr
  else
    return lj_subtype_Var_Left(t1,t2,tds,env,state)
  end
end

# Type(T) is an invariant constructor (TODO: it is more complicated actually)
function lj_subtype(t1::TType, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TType, TType",t1,t2,env)

  new_state = state_set_invariant_position(state)
  return invariance_check(t1.t, t2.t, tds, env, new_state)
end

function lj_subtype_Type_Left(t1::TType, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  # this mimicks the code 872 - 882 of subtype.c, no attempt at understanding the rationale
  if !(isa(t1.t, TVar))
    env_saved = deepcopy(env)
    sr = SR(lj_typeof(t1.t, tds, env_saved) == t2, env)
  else
    sr = SR(false, env)
  end
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::TType, t2::ASTBase, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TType, ASTBase",t1,t2,env)
  lj_subtype_Type_Left(t1,t2,tds,env,state)
end

function lj_subtype(t1::TType, t2::TApp, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TType, TApp",t1,t2,env)
  lj_subtype_Type_Left(t1,t2,tds,env,state)
end

function lj_subtype_Type_Right(t1::ASTBase, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  # this mimicks the code 884 - 896 of subtype.c, no attempt at understanding the rationale

  if !(isa(t2.t, TVar))
    sr = SR(false,env)
    debug_subtype_result(sr)
    return sr
  end

  if !is_kind(t1)
    sr = SR(false,env)
    debug_subtype_result(sr)
    return sr
  end

  new_state = state_set_invariant_position(state)
  
  t2d = env_search(env, t2.t)
  if t2d.ub == TAny()
    # FZN: I believe this call is equivalent to checking that t2d.lb = Bottom
    sr = lj_subtype(TWhere(TType(TVar(:T)), TVar(:T), TUnion([]), TAny()), t2, tds, env, new_state)
  else
    sr = invariance_check(t1,t2.t, tds, env, new_state)
  end
  debug_subtype_result(sr)
  return sr
end

function lj_subtype(t1::ASTBase, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("ASTBase, TType",t1,t2,env)
  lj_subtype_Type_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TName, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TName, TType",t1,t2,env)
  if (string(t1.name) == "TypeofBottom") && t2.t == TUnion([])
    sr = SR(true, env)
    debug_subtype_result(sr)
    return sr
  end
    
  lj_subtype_Type_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TApp, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TApp, TType",t1,t2,env)
  lj_subtype_Type_Right(t1,t2,tds,env,state)
end

function lj_subtype(t1::TVar, t2::TType, tds::TyDeclCol, env::Env, state::ST_State)
  debug_subtype("TVar, TType",t1,t2,env)
  lj_subtype_Var_Left(t1,t2,tds,env,state)
end
